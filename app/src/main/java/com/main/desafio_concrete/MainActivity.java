package com.main.desafio_concrete;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.main.desafio_concrete.presenter.ScreenPresenter;
import com.main.desafio_concrete.presenter.ScreenPresenterImplements;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ProgressDialog  progressDialog;
    private ScreenPresenter screenPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        load();
    }

    private void init(){

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        Button iniciar  = (Button)    findViewById(R.id.activity_fullscreen_iniciar);
        ImageView digiUrl = (ImageView) findViewById(R.id.activity_full_screen_url);

        digiUrl.setOnClickListener(this);
        iniciar.setOnClickListener(this);
    }

    private void load(){
        screenPresenter = new ScreenPresenterImplements(MainActivity.this);
        screenPresenter.registerIservice(iServiceReceiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.activity_fullscreen_iniciar:

                progressDialog.setMessage("Carregando dados");
                progressDialog.show();

                screenPresenter.startIService();

                break;
            case R.id.activity_full_screen_url:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.concrete.com.br")));
                break;
        }
    }
    private BroadcastReceiver iServiceReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {

                String retornoOk   = intent.getStringExtra(getString(R.string.load_profile_iservice_retorno));
                String retornoErro = intent.getStringExtra(getString(R.string.load_profile_iservice_erro));

                if(retornoOk != null){
                    finish();
                }else if (retornoErro != null){
                    new AlertDialog.Builder(MainActivity.this).setMessage("Sem conexão com a internet.").setPositiveButton("OK", null).show();
                }
                progressDialog.dismiss();
            }
            progressDialog.dismiss();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(iServiceReceiver);
    }
}
