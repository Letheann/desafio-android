package com.main.desafio_concrete.service;

import android.os.AsyncTask;
import android.widget.LinearLayout;

import com.github.rafaelcrz.android_endless_scroll_lib.ScrollEndless;
import com.google.gson.Gson;
import com.main.desafio_concrete.adapter.ProfileRecyclerAdapter;
import com.main.desafio_concrete.entity.UserGit;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by bruno on 08/11/2017.
 */

public class AsyncTaskEndless extends AsyncTask<Object, Object, UserGit[]> {


    private int page;
    ProfileRecyclerAdapter adapter;
    ScrollEndless endless;
    LinearLayout layout;

    public AsyncTaskEndless(int page, ProfileRecyclerAdapter adapter, ScrollEndless endless, LinearLayout layout){

        this.page = page;
        this.adapter = adapter;
        this.endless = endless;
        this.layout= layout;

    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected UserGit[] doInBackground(Object... params) {
        UserGit[] arrayprof = new UserGit[0];
        try {
            String retorno = httpRequest(page);

            if (retorno != null) {
                Gson gson = new Gson();

                JSONObject jsonObject = new JSONObject(retorno);

                JSONArray jsonArray = jsonObject.getJSONArray("items");

                arrayprof = gson.fromJson(jsonArray.toString(), UserGit[].class);

            }

        }catch (Exception e)
        {
            e.getMessage().toString();
        }

        return arrayprof;
    }

    public String httpRequest(int page) {
        String resposta = null;
        try {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder().url("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=" + page).build();

            Response response = client.newCall(request).execute();
            resposta = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resposta;
    }


    @Override
    protected void onPostExecute(UserGit[] s) {
        super.onPostExecute(s);


        //// TODO: 08/11/2017 esse aqui deu trabaio hein.
        adapter.addItem(s, adapter.getItemCount());
        //CLose the ProgressDialog
        endless.closeProgressDialog();
        //Setting the Loading in false. The call is complete.
        endless.isLoading(false);
        //First set the next Page

    }
}



