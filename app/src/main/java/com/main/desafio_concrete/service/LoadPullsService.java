package com.main.desafio_concrete.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.main.desafio_concrete.DetalheProprietario;
import com.main.desafio_concrete.LoginActivity;
import com.main.desafio_concrete.R;
import com.main.desafio_concrete.UsersActivity;
import com.main.desafio_concrete.entity.UserGit;
import com.main.desafio_concrete.entity.UserPulls;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by bruno on 09/11/2017.
 */

public class LoadPullsService extends IntentService {

    private String criador;
    private String repositorio;
    public static final String ACTION_FOO2 = "com.main.desafio_concrete.action.FOO2";

    public LoadPullsService() {
        super("LoadPullsService");
    }




    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        criador =(String) intent.getExtras().get("criador");
        repositorio =(String) intent.getExtras().get("repositorio");

        if (intent != null){

            Intent sendIService = new Intent(ACTION_FOO2);

            try{
                String retorno  = httpRequest(criador,repositorio);

                if (retorno != null && retorno.contains("[")){

                    Gson gson = new Gson();

                    UserPulls arrayprof = gson.fromJson(retorno, UserPulls.class);

                    Intent intentActivity = new Intent(getApplicationContext(), DetalheProprietario.class);
                    intentActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intentActivity.putExtra(getString(R.string.load_profile_iservice_key_intent_profile), arrayprof);
                    startActivity(intentActivity);

                    sendIService.putExtra(getString(R.string.load_profile_iservice_retorno), "Ok");
                }else {
                    sendIService.putExtra(getString(R.string.load_profile_iservice_erro), getString(R.string.load_profile_iservice_erro));
                }
            }catch (Exception e){
                sendIService.putExtra(getString(R.string.load_profile_iservice_erro), getString(R.string.load_profile_iservice_erro));
            }finally {
                sendBroadcast(sendIService);
                stopService(new Intent(getApplicationContext(), LoadProfileService.class));
            }

        }
    }


    public String httpRequest(String criador, String repositorio) {
        String resposta = null;
        try {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder().url("https://api.github.com/repos/" + criador + "/" + repositorio +"/pulls/1").build();

            Response response = client.newCall(request).execute();
            resposta = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resposta;
    }
}
