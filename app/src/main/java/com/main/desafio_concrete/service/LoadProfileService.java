package com.main.desafio_concrete.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.main.desafio_concrete.R;
import com.main.desafio_concrete.UsersActivity;
import com.main.desafio_concrete.entity.UserGit;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by bruno on 08/11/2017.
 */

public class LoadProfileService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */

    public static final String ACTION_FOO = "com.main.desafio_concrete.action.FOO";

    public LoadProfileService() {
        super("LoadProfileService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (intent != null){

            Intent sendIService = new Intent(ACTION_FOO);

            try{
                String retorno  = httpRequest(1);

                if (retorno != null){
                    Gson gson = new Gson();

                    JSONObject jsonObject = new JSONObject(retorno);

                    JSONArray jsonArray = jsonObject.getJSONArray("items");

                    UserGit[] arrayprof = gson.fromJson(jsonArray.toString(), UserGit[].class);

                    Intent intentActivity = new Intent(getApplicationContext(), UsersActivity.class);

                    intentActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intentActivity.putExtra(getString(R.string.load_profile_iservice_key_intent_profile), arrayprof);
                    startActivity(intentActivity);

                    sendIService.putExtra(getString(R.string.load_profile_iservice_retorno), "Ok");
                }else {
                    sendIService.putExtra(getString(R.string.load_profile_iservice_erro), getString(R.string.load_profile_iservice_erro));
                }
            }catch (Exception e){
                sendIService.putExtra(getString(R.string.load_profile_iservice_erro), getString(R.string.load_profile_iservice_erro));
            }finally {
                sendBroadcast(sendIService);
                stopService(new Intent(getApplicationContext(), LoadProfileService.class));
            }

        }


    }

    public String httpRequest(int page) {
        String resposta = null;
        try {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder().url("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=" + page).build();

            Response response = client.newCall(request).execute();
            resposta = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resposta;
    }


}
