package com.main.desafio_concrete;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.main.desafio_concrete.entity.UserGit;
import com.main.desafio_concrete.presenter.LoginPresenter;
import com.main.desafio_concrete.presenter.LoginPresenterImplements;

import java.util.List;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String KEY_USER_NAME = "key_user_name";

    private ImageView avatar;
    private TextView  userName;
    private UserGit listprofile;
    private LoginPresenter loginPresenter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
        load();
    }

    public void load() {

        listprofile = (UserGit) getIntent().getSerializableExtra(LoginPresenterImplements.KEY_TAG);
        loginPresenter  = new LoginPresenterImplements(this);
        loginPresenter.registerIservice(iServiceReceiver);
        loginPresenter.setProfile(avatar, userName);

    }

    private void init() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        avatar   = (ImageView) findViewById(R.id.activity_login_img_user);
        userName = (TextView)  findViewById(R.id.activity_login_user_name);

        avatar.setOnClickListener(this);
        userName.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.activity_login_user_name:
            case R.id.activity_login_img_user:

                progressDialog.setMessage("Carregando dados");
                progressDialog.show();


                loginPresenter.startIService(listprofile.getOwner().getLogin(), listprofile.getName());

                break;

        }
    }


    private BroadcastReceiver iServiceReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent != null) {

                String retornoOk   = intent.getStringExtra(getString(R.string.load_profile_iservice_retorno));
                String retornoErro = intent.getStringExtra(getString(R.string.load_profile_iservice_erro));

                if(retornoOk != null){
                    finish();
                }else if (retornoErro != null){
                    new AlertDialog.Builder(LoginActivity.this).setMessage("Sem conexão com a internet.").setPositiveButton("OK", null).show();
                }
                progressDialog.dismiss();
            }
            progressDialog.dismiss();
        }
    };

    @Override
    protected void onStop()
    {
        unregisterReceiver(iServiceReceiver);
        super.onStop();
    }

}
