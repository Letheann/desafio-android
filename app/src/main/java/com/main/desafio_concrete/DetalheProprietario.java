package com.main.desafio_concrete;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.rafaelcrz.android_endless_scroll_lib.ScrollEndless;
import com.main.desafio_concrete.entity.UserGit;
import com.main.desafio_concrete.entity.UserPulls;
import com.main.desafio_concrete.presenter.DetalhesProprietarioImplements;
import com.main.desafio_concrete.presenter.DetalhesProprietarioPresenter;
import com.main.desafio_concrete.presenter.LoginPresenter;
import com.main.desafio_concrete.presenter.LoginPresenterImplements;
import com.main.desafio_concrete.presenter.UsersPresenter;
import com.main.desafio_concrete.presenter.UsersPresenterImplements;

import java.util.Arrays;
import java.util.List;


public class DetalheProprietario extends AppCompatActivity {

    private RecyclerView recyclerView;
    private DetalhesProprietarioPresenter detalhesPresenter;
    private LoginPresenter loginPresenter;
    UserPulls arrayProfile;
    private LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detalhe_proprietario);

        init();
        load();
    }

    private void init() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.activity_detalhes_recyclerview);
        layout = (LinearLayout) findViewById(R.id.activity_detalhes);

    }

    private void load() {

        arrayProfile = (UserPulls) getIntent().getSerializableExtra(getString(R.string.load_profile_iservice_key_intent_profile));

        detalhesPresenter = new DetalhesProprietarioImplements(this, recyclerView);

        detalhesPresenter.order(arrayProfile);

        detalhesPresenter.adapter();







    }

}
