package com.main.desafio_concrete.presenter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.github.rafaelcrz.android_endless_scroll_lib.ScrollEndless;
import com.main.desafio_concrete.adapter.ProfileRecyclerAdapter;
import com.main.desafio_concrete.adapter.ProfilepullsAdapter;
import com.main.desafio_concrete.entity.UserGit;
import com.main.desafio_concrete.entity.UserPulls;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by digi on 09/11/2017.
 */

public class DetalhesProprietarioImplements extends RecyclerView.OnScrollListener implements DetalhesProprietarioPresenter {


    private RecyclerView  recyclerView;
    private List<UserPulls> listProfile;
    private ProfilepullsAdapter adapter;
    private Context context;

    private LinearLayoutManager layoutManager;


    public DetalhesProprietarioImplements(Context context, RecyclerView recyclerView) {

        this.context = context;
        this.recyclerView = recyclerView;
    }



    @Override
    public List<UserPulls> order(UserPulls arrayProfile) {
        listProfile = Arrays.asList(arrayProfile);

        Collections.sort(listProfile, new Comparator<UserPulls>() {
            @Override
            public int compare(UserPulls o1, UserPulls o2) {
                return o1.getUser().getLogin().compareToIgnoreCase(o2.getUser().getLogin());
            }
        });

        return listProfile;

    }



    @Override
    public void adapter() {

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(this);
        adapter = new ProfilepullsAdapter(context, listProfile);
        recyclerView.setAdapter(adapter);


    }
}
