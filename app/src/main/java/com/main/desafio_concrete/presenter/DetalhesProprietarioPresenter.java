package com.main.desafio_concrete.presenter;

import com.main.desafio_concrete.entity.UserPulls;

import java.util.List;

/**
 * Created by digi on 09/11/2017.
 */

public interface DetalhesProprietarioPresenter {
    void adapter();

    List<UserPulls> order(UserPulls arrayProfile);
}
