package com.main.desafio_concrete.presenter;

import android.content.BroadcastReceiver;
import android.widget.ImageView;
import android.widget.TextView;

import com.main.desafio_concrete.entity.*;

/**
 * LoginPresenter
 *
 * Created by bruno on 02/01/2017.
 */
public interface LoginPresenter {

    void setProfile(ImageView avatar, TextView userName);

    UserGit getProfile();

    void startIService(String login, String name);


    void registerIservice(BroadcastReceiver iServiceReceiver);
}
