package com.main.desafio_concrete.presenter;

import android.content.Context;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.github.rafaelcrz.android_endless_scroll_lib.ScrollEndless;
import com.main.desafio_concrete.adapter.ProfileRecyclerAdapter;
import com.main.desafio_concrete.entity.UserGit;
import com.main.desafio_concrete.service.AsyncTaskEndless;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * Created by bruno on 08/11/2017.
 */

public class UsersPresenterImplements extends RecyclerView.OnScrollListener implements UsersPresenter {

    private RecyclerView  recyclerView;
    private List<UserGit> listProfile;
    private ScrollEndless endless;
    private LinearLayout layout;
    private ProfileRecyclerAdapter adapter;
    private Context context;


    private int page = 1;
    private LinearLayoutManager layoutManager;

    public UsersPresenterImplements(Context context, RecyclerView recyclerView, ScrollEndless endless, LinearLayout layout){
        this.context = context;
        this.recyclerView = recyclerView;
        this.endless = endless;
        this.layout = layout;

    }


    @Override
    public List<UserGit> order(UserGit[] arrayProfile) {
        listProfile = Arrays.asList(arrayProfile);

        Collections.sort(listProfile, new Comparator<UserGit>() {
            @Override
            public int compare(UserGit o1, UserGit o2) {
                return o1.getFullName().compareToIgnoreCase(o2.getFullName());
            }
        });

        return listProfile;

    }

    @Override
    public void adapter() {

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(this);
        adapter = new ProfileRecyclerAdapter(context, listProfile);
        recyclerView.setAdapter(adapter);

        endless = new ScrollEndless(context, recyclerView, layoutManager);



        if (listProfile != null){
            // TODO: 09/11/2017 apenas limitando requisições para entrar na condição onLoadAllFinish
            endless.setTotalPage(10);

        }else{
            makeCallSample();
        }


        endless.addScrollEndless(new ScrollEndless.EndlessScrollListener() {
            @Override
            public void onLoadMore() {

                page = endless.getPage() + 1;

                endless.setPage(page);

                endless.isLoading(true);

                endless.showProgressDialog("Carregando...", "Aguarde", false);


                makeCallSample();

            }

            @Override
            public void onLoadAllFinish() {

                Snackbar snackbar = Snackbar.make(layout, "Todos itens já carregados!", Snackbar.LENGTH_SHORT);
                snackbar.show();
            }
        });
    }


    //Simule a request call
    private void makeCallSample() {
        AsyncTask a = new AsyncTaskEndless(page, adapter, endless, layout);
        a.execute();

    }

}



