package com.main.desafio_concrete.presenter;

import com.main.desafio_concrete.entity.UserGit;

import java.util.List;

/**
 * Created by bruno on 08/11/2017.
 */

public interface UsersPresenter {
    List<UserGit> order(UserGit[] arrayProfile);

    void adapter();

}
