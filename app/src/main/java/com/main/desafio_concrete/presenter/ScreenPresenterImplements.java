package com.main.desafio_concrete.presenter;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;

import com.main.desafio_concrete.MainActivity;
import com.main.desafio_concrete.service.LoadProfileService;

/**
 * ApresentadorImplementar
 *
 * Created by bruno on 08/11/2017.
 */

public class ScreenPresenterImplements implements ScreenPresenter {

    private MainActivity context;

    public ScreenPresenterImplements(MainActivity context){
        this.context = context;
    }

    @Override
    public void startIService() {
        Intent intent = new Intent(context, LoadProfileService.class);
        context.startService(intent);
    }

    @Override
    public void registerIservice(BroadcastReceiver iServiceReceiver) {
        context.registerReceiver(iServiceReceiver, new IntentFilter(LoadProfileService.ACTION_FOO));
    }
}
