package com.main.desafio_concrete.presenter;

import android.content.BroadcastReceiver;

/**
 * Apresentador
 *
 * Created by bruno on 08/11/2017.
 */

public interface ScreenPresenter {
    void startIService();

    void registerIservice(BroadcastReceiver iServiceReceiver);
}
