package com.main.desafio_concrete.presenter;


import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.ImageView;
import android.widget.TextView;

import com.main.desafio_concrete.LoginActivity;
import com.main.desafio_concrete.entity.UserGit;
import com.main.desafio_concrete.service.LoadProfileService;
import com.main.desafio_concrete.service.LoadPullsService;
import com.main.desafio_concrete.view.RoundedTransformation;
import com.squareup.picasso.Picasso;


/**
 *
 *
 * Created by bruno on 02/01/2017.
 */

public class LoginPresenterImplements implements LoginPresenter {

    public static final String KEY_TAG = "key_tag";

    private LoginActivity loginActivity;

    public LoginPresenterImplements(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
    }

    @Override
    public void setProfile(ImageView avatar, TextView userName) {
        UserGit profile = getProfile();

        Picasso.with(loginActivity).load(profile.getOwner().getAvatarUrl())
                .transform(new RoundedTransformation())
                .resize(250, 250)
                .centerCrop().into(avatar);

        userName.setText(profile.getOwner().getLogin());

    }

    @Override
    public UserGit getProfile() {
        return (UserGit) loginActivity.getIntent().getSerializableExtra(KEY_TAG);
    }

    @Override
    public void startIService(String criador, String repositorio) {

        Intent intent = new Intent(loginActivity, LoadPullsService.class);
        intent.putExtra("criador", criador);
        intent.putExtra("repositorio", repositorio);

        loginActivity.startService(intent);
    }

    @Override
    public void registerIservice(BroadcastReceiver iServiceReceiver) {
        loginActivity.registerReceiver(iServiceReceiver, new IntentFilter(LoadPullsService.ACTION_FOO2));
    }


}
