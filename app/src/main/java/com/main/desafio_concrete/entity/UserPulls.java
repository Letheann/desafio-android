package com.main.desafio_concrete.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by digi on 09/11/2017.
 */

public class UserPulls implements Serializable{


    /**
     * url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/1
     * id : 20325074
     * html_url : https://github.com/ReactiveX/BuildInfrastructure/pull/1
     * diff_url : https://github.com/ReactiveX/BuildInfrastructure/pull/1.diff
     * patch_url : https://github.com/ReactiveX/BuildInfrastructure/pull/1.patch
     * issue_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/1
     * number : 1
     * state : closed
     * locked : false
     * title : We shouldn't need the repo directly anymore
     * user : {"login":"quidryan","id":360255,"avatar_url":"https://avatars0.githubusercontent.com/u/360255?v=4","gravatar_id":"","url":"https://api.github.com/users/quidryan","html_url":"https://github.com/quidryan","followers_url":"https://api.github.com/users/quidryan/followers","following_url":"https://api.github.com/users/quidryan/following{/other_user}","gists_url":"https://api.github.com/users/quidryan/gists{/gist_id}","starred_url":"https://api.github.com/users/quidryan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/quidryan/subscriptions","organizations_url":"https://api.github.com/users/quidryan/orgs","repos_url":"https://api.github.com/users/quidryan/repos","events_url":"https://api.github.com/users/quidryan/events{/privacy}","received_events_url":"https://api.github.com/users/quidryan/received_events","type":"User","site_admin":false}
     * body : Some one has to be first.

     * created_at : 2014-08-26T17:47:53Z
     * updated_at : 2014-08-26T18:13:56Z
     * closed_at : 2014-08-26T18:13:54Z
     * merged_at : 2014-08-26T18:13:54Z
     * merge_commit_sha : 7d2931986aa27c163196676ad42dbfcb3c936e62
     * assignee : null
     * assignees : []
     * requested_reviewers : []
     * milestone : null
     * commits_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/1/commits
     * review_comments_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/1/comments
     * review_comment_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/comments{/number}
     * comments_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/1/comments
     * statuses_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/statuses/ac0e97acc6e69ae7554eaff4c35a3214825761fb
     * head : {"label":"quidryan:feature/first","ref":"feature/first","sha":"ac0e97acc6e69ae7554eaff4c35a3214825761fb","user":{"login":"quidryan","id":360255,"avatar_url":"https://avatars0.githubusercontent.com/u/360255?v=4","gravatar_id":"","url":"https://api.github.com/users/quidryan","html_url":"https://github.com/quidryan","followers_url":"https://api.github.com/users/quidryan/followers","following_url":"https://api.github.com/users/quidryan/following{/other_user}","gists_url":"https://api.github.com/users/quidryan/gists{/gist_id}","starred_url":"https://api.github.com/users/quidryan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/quidryan/subscriptions","organizations_url":"https://api.github.com/users/quidryan/orgs","repos_url":"https://api.github.com/users/quidryan/repos","events_url":"https://api.github.com/users/quidryan/events{/privacy}","received_events_url":"https://api.github.com/users/quidryan/received_events","type":"User","site_admin":false},"repo":{"id":23360997,"name":"BuildInfrastructure","full_name":"quidryan/BuildInfrastructure","owner":{"login":"quidryan","id":360255,"avatar_url":"https://avatars0.githubusercontent.com/u/360255?v=4","gravatar_id":"","url":"https://api.github.com/users/quidryan","html_url":"https://github.com/quidryan","followers_url":"https://api.github.com/users/quidryan/followers","following_url":"https://api.github.com/users/quidryan/following{/other_user}","gists_url":"https://api.github.com/users/quidryan/gists{/gist_id}","starred_url":"https://api.github.com/users/quidryan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/quidryan/subscriptions","organizations_url":"https://api.github.com/users/quidryan/orgs","repos_url":"https://api.github.com/users/quidryan/repos","events_url":"https://api.github.com/users/quidryan/events{/privacy}","received_events_url":"https://api.github.com/users/quidryan/received_events","type":"User","site_admin":false},"private":false,"html_url":"https://github.com/quidryan/BuildInfrastructure","description":"Test project for the new build system.","fork":true,"url":"https://api.github.com/repos/quidryan/BuildInfrastructure","forks_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/forks","keys_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/keys{/key_id}","collaborators_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/collaborators{/collaborator}","teams_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/teams","hooks_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/hooks","issue_events_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/issues/events{/number}","events_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/events","assignees_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/assignees{/user}","branches_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/branches{/branch}","tags_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/tags","blobs_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/git/blobs{/sha}","git_tags_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/git/tags{/sha}","git_refs_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/git/refs{/sha}","trees_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/git/trees{/sha}","statuses_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/statuses/{sha}","languages_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/languages","stargazers_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/stargazers","contributors_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/contributors","subscribers_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/subscribers","subscription_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/subscription","commits_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/commits{/sha}","git_commits_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/git/commits{/sha}","comments_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/comments{/number}","issue_comment_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/issues/comments{/number}","contents_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/contents/{+path}","compare_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/compare/{base}...{head}","merges_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/merges","archive_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/{archive_format}{/ref}","downloads_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/downloads","issues_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/issues{/number}","pulls_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/pulls{/number}","milestones_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/milestones{/number}","notifications_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/notifications{?since,all,participating}","labels_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/labels{/name}","releases_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/releases{/id}","deployments_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/deployments","created_at":"2014-08-26T17:46:20Z","updated_at":"2014-08-20T20:03:08Z","pushed_at":"2014-08-26T18:13:56Z","git_url":"git://github.com/quidryan/BuildInfrastructure.git","ssh_url":"git@github.com:quidryan/BuildInfrastructure.git","clone_url":"https://github.com/quidryan/BuildInfrastructure.git","svn_url":"https://github.com/quidryan/BuildInfrastructure","homepage":null,"size":145,"stargazers_count":0,"watchers_count":0,"language":null,"has_issues":false,"has_projects":true,"has_downloads":true,"has_wiki":false,"has_pages":false,"forks_count":0,"mirror_url":null,"archived":false,"open_issues_count":0,"forks":0,"open_issues":0,"watchers":0,"default_branch":"master"}}
     * base : {"label":"ReactiveX:master","ref":"master","sha":"39e6f7d128879828137e310e3811559efa8fe8ba","user":{"login":"ReactiveX","id":6407041,"avatar_url":"https://avatars1.githubusercontent.com/u/6407041?v=4","gravatar_id":"","url":"https://api.github.com/users/ReactiveX","html_url":"https://github.com/ReactiveX","followers_url":"https://api.github.com/users/ReactiveX/followers","following_url":"https://api.github.com/users/ReactiveX/following{/other_user}","gists_url":"https://api.github.com/users/ReactiveX/gists{/gist_id}","starred_url":"https://api.github.com/users/ReactiveX/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ReactiveX/subscriptions","organizations_url":"https://api.github.com/users/ReactiveX/orgs","repos_url":"https://api.github.com/users/ReactiveX/repos","events_url":"https://api.github.com/users/ReactiveX/events{/privacy}","received_events_url":"https://api.github.com/users/ReactiveX/received_events","type":"Organization","site_admin":false},"repo":{"id":23123948,"name":"BuildInfrastructure","full_name":"ReactiveX/BuildInfrastructure","owner":{"login":"ReactiveX","id":6407041,"avatar_url":"https://avatars1.githubusercontent.com/u/6407041?v=4","gravatar_id":"","url":"https://api.github.com/users/ReactiveX","html_url":"https://github.com/ReactiveX","followers_url":"https://api.github.com/users/ReactiveX/followers","following_url":"https://api.github.com/users/ReactiveX/following{/other_user}","gists_url":"https://api.github.com/users/ReactiveX/gists{/gist_id}","starred_url":"https://api.github.com/users/ReactiveX/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ReactiveX/subscriptions","organizations_url":"https://api.github.com/users/ReactiveX/orgs","repos_url":"https://api.github.com/users/ReactiveX/repos","events_url":"https://api.github.com/users/ReactiveX/events{/privacy}","received_events_url":"https://api.github.com/users/ReactiveX/received_events","type":"Organization","site_admin":false},"private":false,"html_url":"https://github.com/ReactiveX/BuildInfrastructure","description":"Test project for the new build system.","fork":false,"url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure","forks_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/forks","keys_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/keys{/key_id}","collaborators_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/collaborators{/collaborator}","teams_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/teams","hooks_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/hooks","issue_events_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/events{/number}","events_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/events","assignees_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/assignees{/user}","branches_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/branches{/branch}","tags_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/tags","blobs_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/blobs{/sha}","git_tags_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/tags{/sha}","git_refs_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/refs{/sha}","trees_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/trees{/sha}","statuses_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/statuses/{sha}","languages_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/languages","stargazers_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/stargazers","contributors_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/contributors","subscribers_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/subscribers","subscription_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/subscription","commits_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/commits{/sha}","git_commits_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/commits{/sha}","comments_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/comments{/number}","issue_comment_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/comments{/number}","contents_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/contents/{+path}","compare_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/compare/{base}...{head}","merges_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/merges","archive_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/{archive_format}{/ref}","downloads_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/downloads","issues_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues{/number}","pulls_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls{/number}","milestones_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/milestones{/number}","notifications_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/notifications{?since,all,participating}","labels_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/labels{/name}","releases_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/releases{/id}","deployments_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/deployments","created_at":"2014-08-19T20:03:00Z","updated_at":"2017-11-07T09:25:51Z","pushed_at":"2015-01-19T21:55:02Z","git_url":"git://github.com/ReactiveX/BuildInfrastructure.git","ssh_url":"git@github.com:ReactiveX/BuildInfrastructure.git","clone_url":"https://github.com/ReactiveX/BuildInfrastructure.git","svn_url":"https://github.com/ReactiveX/BuildInfrastructure","homepage":null,"size":682,"stargazers_count":1,"watchers_count":1,"language":"Groovy","has_issues":true,"has_projects":true,"has_downloads":true,"has_wiki":false,"has_pages":false,"forks_count":7,"mirror_url":null,"archived":false,"open_issues_count":0,"forks":7,"open_issues":0,"watchers":1,"default_branch":"master"}}
     * _links : {"self":{"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/1"},"html":{"href":"https://github.com/ReactiveX/BuildInfrastructure/pull/1"},"issue":{"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/1"},"comments":{"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/1/comments"},"review_comments":{"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/1/comments"},"review_comment":{"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/comments{/number}"},"commits":{"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/1/commits"},"statuses":{"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/statuses/ac0e97acc6e69ae7554eaff4c35a3214825761fb"}}
     * author_association : COLLABORATOR
     * merged : true
     * mergeable : null
     * rebaseable : null
     * mergeable_state : unknown
     * merged_by : {"login":"quidryan","id":360255,"avatar_url":"https://avatars0.githubusercontent.com/u/360255?v=4","gravatar_id":"","url":"https://api.github.com/users/quidryan","html_url":"https://github.com/quidryan","followers_url":"https://api.github.com/users/quidryan/followers","following_url":"https://api.github.com/users/quidryan/following{/other_user}","gists_url":"https://api.github.com/users/quidryan/gists{/gist_id}","starred_url":"https://api.github.com/users/quidryan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/quidryan/subscriptions","organizations_url":"https://api.github.com/users/quidryan/orgs","repos_url":"https://api.github.com/users/quidryan/repos","events_url":"https://api.github.com/users/quidryan/events{/privacy}","received_events_url":"https://api.github.com/users/quidryan/received_events","type":"User","site_admin":false}
     * comments : 7
     * review_comments : 0
     * maintainer_can_modify : false
     * commits : 4
     * additions : 13
     * deletions : 1
     * changed_files : 3
     */

    @SerializedName("url")
    private String url;
    @SerializedName("id")
    private int id;
    @SerializedName("html_url")
    private String htmlUrl;
    @SerializedName("diff_url")
    private String diffUrl;
    @SerializedName("patch_url")
    private String patchUrl;
    @SerializedName("issue_url")
    private String issueUrl;
    @SerializedName("number")
    private int number;
    @SerializedName("state")
    private String state;
    @SerializedName("locked")
    private boolean locked;
    @SerializedName("title")
    private String title;
    @SerializedName("user")
    private UserBean user;
    @SerializedName("body")
    private String body;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("closed_at")
    private String closedAt;
    @SerializedName("merged_at")
    private String mergedAt;
    @SerializedName("merge_commit_sha")
    private String mergeCommitSha;
    @SerializedName("assignee")
    private Object assignee;
    @SerializedName("milestone")
    private Object milestone;
    @SerializedName("commits_url")
    private String commitsUrl;
    @SerializedName("review_comments_url")
    private String reviewCommentsUrl;
    @SerializedName("review_comment_url")
    private String reviewCommentUrl;
    @SerializedName("comments_url")
    private String commentsUrl;
    @SerializedName("statuses_url")
    private String statusesUrl;
    @SerializedName("head")
    private HeadBean head;
    @SerializedName("base")
    private BaseBean base;
    @SerializedName("_links")
    private LinksBean links;
    @SerializedName("author_association")
    private String authorAssociation;
    @SerializedName("merged")
    private boolean merged;
    @SerializedName("mergeable")
    private Object mergeable;
    @SerializedName("rebaseable")
    private Object rebaseable;
    @SerializedName("mergeable_state")
    private String mergeableState;
    @SerializedName("merged_by")
    private MergedByBean mergedBy;
    @SerializedName("comments")
    private int comments;
    @SerializedName("review_comments")
    private int reviewComments;
    @SerializedName("maintainer_can_modify")
    private boolean maintainerCanModify;
    @SerializedName("commits")
    private int commits;
    @SerializedName("additions")
    private int additions;
    @SerializedName("deletions")
    private int deletions;
    @SerializedName("changed_files")
    private int changedFiles;
    @SerializedName("assignees")
    private List<?> assignees;
    @SerializedName("requested_reviewers")
    private List<?> requestedReviewers;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public String getDiffUrl() {
        return diffUrl;
    }

    public void setDiffUrl(String diffUrl) {
        this.diffUrl = diffUrl;
    }

    public String getPatchUrl() {
        return patchUrl;
    }

    public void setPatchUrl(String patchUrl) {
        this.patchUrl = patchUrl;
    }

    public String getIssueUrl() {
        return issueUrl;
    }

    public void setIssueUrl(String issueUrl) {
        this.issueUrl = issueUrl;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getClosedAt() {
        return closedAt;
    }

    public void setClosedAt(String closedAt) {
        this.closedAt = closedAt;
    }

    public String getMergedAt() {
        return mergedAt;
    }

    public void setMergedAt(String mergedAt) {
        this.mergedAt = mergedAt;
    }

    public String getMergeCommitSha() {
        return mergeCommitSha;
    }

    public void setMergeCommitSha(String mergeCommitSha) {
        this.mergeCommitSha = mergeCommitSha;
    }

    public Object getAssignee() {
        return assignee;
    }

    public void setAssignee(Object assignee) {
        this.assignee = assignee;
    }

    public Object getMilestone() {
        return milestone;
    }

    public void setMilestone(Object milestone) {
        this.milestone = milestone;
    }

    public String getCommitsUrl() {
        return commitsUrl;
    }

    public void setCommitsUrl(String commitsUrl) {
        this.commitsUrl = commitsUrl;
    }

    public String getReviewCommentsUrl() {
        return reviewCommentsUrl;
    }

    public void setReviewCommentsUrl(String reviewCommentsUrl) {
        this.reviewCommentsUrl = reviewCommentsUrl;
    }

    public String getReviewCommentUrl() {
        return reviewCommentUrl;
    }

    public void setReviewCommentUrl(String reviewCommentUrl) {
        this.reviewCommentUrl = reviewCommentUrl;
    }

    public String getCommentsUrl() {
        return commentsUrl;
    }

    public void setCommentsUrl(String commentsUrl) {
        this.commentsUrl = commentsUrl;
    }

    public String getStatusesUrl() {
        return statusesUrl;
    }

    public void setStatusesUrl(String statusesUrl) {
        this.statusesUrl = statusesUrl;
    }

    public HeadBean getHead() {
        return head;
    }

    public void setHead(HeadBean head) {
        this.head = head;
    }

    public BaseBean getBase() {
        return base;
    }

    public void setBase(BaseBean base) {
        this.base = base;
    }

    public LinksBean getLinks() {
        return links;
    }

    public void setLinks(LinksBean links) {
        this.links = links;
    }

    public String getAuthorAssociation() {
        return authorAssociation;
    }

    public void setAuthorAssociation(String authorAssociation) {
        this.authorAssociation = authorAssociation;
    }

    public boolean isMerged() {
        return merged;
    }

    public void setMerged(boolean merged) {
        this.merged = merged;
    }

    public Object getMergeable() {
        return mergeable;
    }

    public void setMergeable(Object mergeable) {
        this.mergeable = mergeable;
    }

    public Object getRebaseable() {
        return rebaseable;
    }

    public void setRebaseable(Object rebaseable) {
        this.rebaseable = rebaseable;
    }

    public String getMergeableState() {
        return mergeableState;
    }

    public void setMergeableState(String mergeableState) {
        this.mergeableState = mergeableState;
    }

    public MergedByBean getMergedBy() {
        return mergedBy;
    }

    public void setMergedBy(MergedByBean mergedBy) {
        this.mergedBy = mergedBy;
    }

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }

    public int getReviewComments() {
        return reviewComments;
    }

    public void setReviewComments(int reviewComments) {
        this.reviewComments = reviewComments;
    }

    public boolean isMaintainerCanModify() {
        return maintainerCanModify;
    }

    public void setMaintainerCanModify(boolean maintainerCanModify) {
        this.maintainerCanModify = maintainerCanModify;
    }

    public int getCommits() {
        return commits;
    }

    public void setCommits(int commits) {
        this.commits = commits;
    }

    public int getAdditions() {
        return additions;
    }

    public void setAdditions(int additions) {
        this.additions = additions;
    }

    public int getDeletions() {
        return deletions;
    }

    public void setDeletions(int deletions) {
        this.deletions = deletions;
    }

    public int getChangedFiles() {
        return changedFiles;
    }

    public void setChangedFiles(int changedFiles) {
        this.changedFiles = changedFiles;
    }

    public List<?> getAssignees() {
        return assignees;
    }

    public void setAssignees(List<?> assignees) {
        this.assignees = assignees;
    }

    public List<?> getRequestedReviewers() {
        return requestedReviewers;
    }

    public void setRequestedReviewers(List<?> requestedReviewers) {
        this.requestedReviewers = requestedReviewers;
    }

    public static class UserBean implements Serializable {
        /**
         * login : quidryan
         * id : 360255
         * avatar_url : https://avatars0.githubusercontent.com/u/360255?v=4
         * gravatar_id :
         * url : https://api.github.com/users/quidryan
         * html_url : https://github.com/quidryan
         * followers_url : https://api.github.com/users/quidryan/followers
         * following_url : https://api.github.com/users/quidryan/following{/other_user}
         * gists_url : https://api.github.com/users/quidryan/gists{/gist_id}
         * starred_url : https://api.github.com/users/quidryan/starred{/owner}{/repo}
         * subscriptions_url : https://api.github.com/users/quidryan/subscriptions
         * organizations_url : https://api.github.com/users/quidryan/orgs
         * repos_url : https://api.github.com/users/quidryan/repos
         * events_url : https://api.github.com/users/quidryan/events{/privacy}
         * received_events_url : https://api.github.com/users/quidryan/received_events
         * type : User
         * site_admin : false
         */

        @SerializedName("login")
        private String login;
        @SerializedName("id")
        private int id;
        @SerializedName("avatar_url")
        private String avatarUrl;
        @SerializedName("gravatar_id")
        private String gravatarId;
        @SerializedName("url")
        private String url;
        @SerializedName("html_url")
        private String htmlUrl;
        @SerializedName("followers_url")
        private String followersUrl;
        @SerializedName("following_url")
        private String followingUrl;
        @SerializedName("gists_url")
        private String gistsUrl;
        @SerializedName("starred_url")
        private String starredUrl;
        @SerializedName("subscriptions_url")
        private String subscriptionsUrl;
        @SerializedName("organizations_url")
        private String organizationsUrl;
        @SerializedName("repos_url")
        private String reposUrl;
        @SerializedName("events_url")
        private String eventsUrl;
        @SerializedName("received_events_url")
        private String receivedEventsUrl;
        @SerializedName("type")
        private String type;
        @SerializedName("site_admin")
        private boolean siteAdmin;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }

        public void setAvatarUrl(String avatarUrl) {
            this.avatarUrl = avatarUrl;
        }

        public String getGravatarId() {
            return gravatarId;
        }

        public void setGravatarId(String gravatarId) {
            this.gravatarId = gravatarId;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getHtmlUrl() {
            return htmlUrl;
        }

        public void setHtmlUrl(String htmlUrl) {
            this.htmlUrl = htmlUrl;
        }

        public String getFollowersUrl() {
            return followersUrl;
        }

        public void setFollowersUrl(String followersUrl) {
            this.followersUrl = followersUrl;
        }

        public String getFollowingUrl() {
            return followingUrl;
        }

        public void setFollowingUrl(String followingUrl) {
            this.followingUrl = followingUrl;
        }

        public String getGistsUrl() {
            return gistsUrl;
        }

        public void setGistsUrl(String gistsUrl) {
            this.gistsUrl = gistsUrl;
        }

        public String getStarredUrl() {
            return starredUrl;
        }

        public void setStarredUrl(String starredUrl) {
            this.starredUrl = starredUrl;
        }

        public String getSubscriptionsUrl() {
            return subscriptionsUrl;
        }

        public void setSubscriptionsUrl(String subscriptionsUrl) {
            this.subscriptionsUrl = subscriptionsUrl;
        }

        public String getOrganizationsUrl() {
            return organizationsUrl;
        }

        public void setOrganizationsUrl(String organizationsUrl) {
            this.organizationsUrl = organizationsUrl;
        }

        public String getReposUrl() {
            return reposUrl;
        }

        public void setReposUrl(String reposUrl) {
            this.reposUrl = reposUrl;
        }

        public String getEventsUrl() {
            return eventsUrl;
        }

        public void setEventsUrl(String eventsUrl) {
            this.eventsUrl = eventsUrl;
        }

        public String getReceivedEventsUrl() {
            return receivedEventsUrl;
        }

        public void setReceivedEventsUrl(String receivedEventsUrl) {
            this.receivedEventsUrl = receivedEventsUrl;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public boolean isSiteAdmin() {
            return siteAdmin;
        }

        public void setSiteAdmin(boolean siteAdmin) {
            this.siteAdmin = siteAdmin;
        }
    }

    public static class HeadBean implements Serializable {
        /**
         * label : quidryan:feature/first
         * ref : feature/first
         * sha : ac0e97acc6e69ae7554eaff4c35a3214825761fb
         * user : {"login":"quidryan","id":360255,"avatar_url":"https://avatars0.githubusercontent.com/u/360255?v=4","gravatar_id":"","url":"https://api.github.com/users/quidryan","html_url":"https://github.com/quidryan","followers_url":"https://api.github.com/users/quidryan/followers","following_url":"https://api.github.com/users/quidryan/following{/other_user}","gists_url":"https://api.github.com/users/quidryan/gists{/gist_id}","starred_url":"https://api.github.com/users/quidryan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/quidryan/subscriptions","organizations_url":"https://api.github.com/users/quidryan/orgs","repos_url":"https://api.github.com/users/quidryan/repos","events_url":"https://api.github.com/users/quidryan/events{/privacy}","received_events_url":"https://api.github.com/users/quidryan/received_events","type":"User","site_admin":false}
         * repo : {"id":23360997,"name":"BuildInfrastructure","full_name":"quidryan/BuildInfrastructure","owner":{"login":"quidryan","id":360255,"avatar_url":"https://avatars0.githubusercontent.com/u/360255?v=4","gravatar_id":"","url":"https://api.github.com/users/quidryan","html_url":"https://github.com/quidryan","followers_url":"https://api.github.com/users/quidryan/followers","following_url":"https://api.github.com/users/quidryan/following{/other_user}","gists_url":"https://api.github.com/users/quidryan/gists{/gist_id}","starred_url":"https://api.github.com/users/quidryan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/quidryan/subscriptions","organizations_url":"https://api.github.com/users/quidryan/orgs","repos_url":"https://api.github.com/users/quidryan/repos","events_url":"https://api.github.com/users/quidryan/events{/privacy}","received_events_url":"https://api.github.com/users/quidryan/received_events","type":"User","site_admin":false},"private":false,"html_url":"https://github.com/quidryan/BuildInfrastructure","description":"Test project for the new build system.","fork":true,"url":"https://api.github.com/repos/quidryan/BuildInfrastructure","forks_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/forks","keys_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/keys{/key_id}","collaborators_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/collaborators{/collaborator}","teams_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/teams","hooks_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/hooks","issue_events_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/issues/events{/number}","events_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/events","assignees_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/assignees{/user}","branches_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/branches{/branch}","tags_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/tags","blobs_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/git/blobs{/sha}","git_tags_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/git/tags{/sha}","git_refs_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/git/refs{/sha}","trees_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/git/trees{/sha}","statuses_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/statuses/{sha}","languages_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/languages","stargazers_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/stargazers","contributors_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/contributors","subscribers_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/subscribers","subscription_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/subscription","commits_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/commits{/sha}","git_commits_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/git/commits{/sha}","comments_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/comments{/number}","issue_comment_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/issues/comments{/number}","contents_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/contents/{+path}","compare_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/compare/{base}...{head}","merges_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/merges","archive_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/{archive_format}{/ref}","downloads_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/downloads","issues_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/issues{/number}","pulls_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/pulls{/number}","milestones_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/milestones{/number}","notifications_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/notifications{?since,all,participating}","labels_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/labels{/name}","releases_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/releases{/id}","deployments_url":"https://api.github.com/repos/quidryan/BuildInfrastructure/deployments","created_at":"2014-08-26T17:46:20Z","updated_at":"2014-08-20T20:03:08Z","pushed_at":"2014-08-26T18:13:56Z","git_url":"git://github.com/quidryan/BuildInfrastructure.git","ssh_url":"git@github.com:quidryan/BuildInfrastructure.git","clone_url":"https://github.com/quidryan/BuildInfrastructure.git","svn_url":"https://github.com/quidryan/BuildInfrastructure","homepage":null,"size":145,"stargazers_count":0,"watchers_count":0,"language":null,"has_issues":false,"has_projects":true,"has_downloads":true,"has_wiki":false,"has_pages":false,"forks_count":0,"mirror_url":null,"archived":false,"open_issues_count":0,"forks":0,"open_issues":0,"watchers":0,"default_branch":"master"}
         */

        @SerializedName("label")
        private String label;
        @SerializedName("ref")
        private String ref;
        @SerializedName("sha")
        private String sha;
        @SerializedName("user")
        private UserBeanX user;
        @SerializedName("repo")
        private RepoBean repo;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getRef() {
            return ref;
        }

        public void setRef(String ref) {
            this.ref = ref;
        }

        public String getSha() {
            return sha;
        }

        public void setSha(String sha) {
            this.sha = sha;
        }

        public UserBeanX getUser() {
            return user;
        }

        public void setUser(UserBeanX user) {
            this.user = user;
        }

        public RepoBean getRepo() {
            return repo;
        }

        public void setRepo(RepoBean repo) {
            this.repo = repo;
        }

        public static class UserBeanX implements Serializable{
            /**
             * login : quidryan
             * id : 360255
             * avatar_url : https://avatars0.githubusercontent.com/u/360255?v=4
             * gravatar_id :
             * url : https://api.github.com/users/quidryan
             * html_url : https://github.com/quidryan
             * followers_url : https://api.github.com/users/quidryan/followers
             * following_url : https://api.github.com/users/quidryan/following{/other_user}
             * gists_url : https://api.github.com/users/quidryan/gists{/gist_id}
             * starred_url : https://api.github.com/users/quidryan/starred{/owner}{/repo}
             * subscriptions_url : https://api.github.com/users/quidryan/subscriptions
             * organizations_url : https://api.github.com/users/quidryan/orgs
             * repos_url : https://api.github.com/users/quidryan/repos
             * events_url : https://api.github.com/users/quidryan/events{/privacy}
             * received_events_url : https://api.github.com/users/quidryan/received_events
             * type : User
             * site_admin : false
             */

            @SerializedName("login")
            private String login;
            @SerializedName("id")
            private int id;
            @SerializedName("avatar_url")
            private String avatarUrl;
            @SerializedName("gravatar_id")
            private String gravatarId;
            @SerializedName("url")
            private String url;
            @SerializedName("html_url")
            private String htmlUrl;
            @SerializedName("followers_url")
            private String followersUrl;
            @SerializedName("following_url")
            private String followingUrl;
            @SerializedName("gists_url")
            private String gistsUrl;
            @SerializedName("starred_url")
            private String starredUrl;
            @SerializedName("subscriptions_url")
            private String subscriptionsUrl;
            @SerializedName("organizations_url")
            private String organizationsUrl;
            @SerializedName("repos_url")
            private String reposUrl;
            @SerializedName("events_url")
            private String eventsUrl;
            @SerializedName("received_events_url")
            private String receivedEventsUrl;
            @SerializedName("type")
            private String type;
            @SerializedName("site_admin")
            private boolean siteAdmin;

            public String getLogin() {
                return login;
            }

            public void setLogin(String login) {
                this.login = login;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getAvatarUrl() {
                return avatarUrl;
            }

            public void setAvatarUrl(String avatarUrl) {
                this.avatarUrl = avatarUrl;
            }

            public String getGravatarId() {
                return gravatarId;
            }

            public void setGravatarId(String gravatarId) {
                this.gravatarId = gravatarId;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getHtmlUrl() {
                return htmlUrl;
            }

            public void setHtmlUrl(String htmlUrl) {
                this.htmlUrl = htmlUrl;
            }

            public String getFollowersUrl() {
                return followersUrl;
            }

            public void setFollowersUrl(String followersUrl) {
                this.followersUrl = followersUrl;
            }

            public String getFollowingUrl() {
                return followingUrl;
            }

            public void setFollowingUrl(String followingUrl) {
                this.followingUrl = followingUrl;
            }

            public String getGistsUrl() {
                return gistsUrl;
            }

            public void setGistsUrl(String gistsUrl) {
                this.gistsUrl = gistsUrl;
            }

            public String getStarredUrl() {
                return starredUrl;
            }

            public void setStarredUrl(String starredUrl) {
                this.starredUrl = starredUrl;
            }

            public String getSubscriptionsUrl() {
                return subscriptionsUrl;
            }

            public void setSubscriptionsUrl(String subscriptionsUrl) {
                this.subscriptionsUrl = subscriptionsUrl;
            }

            public String getOrganizationsUrl() {
                return organizationsUrl;
            }

            public void setOrganizationsUrl(String organizationsUrl) {
                this.organizationsUrl = organizationsUrl;
            }

            public String getReposUrl() {
                return reposUrl;
            }

            public void setReposUrl(String reposUrl) {
                this.reposUrl = reposUrl;
            }

            public String getEventsUrl() {
                return eventsUrl;
            }

            public void setEventsUrl(String eventsUrl) {
                this.eventsUrl = eventsUrl;
            }

            public String getReceivedEventsUrl() {
                return receivedEventsUrl;
            }

            public void setReceivedEventsUrl(String receivedEventsUrl) {
                this.receivedEventsUrl = receivedEventsUrl;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public boolean isSiteAdmin() {
                return siteAdmin;
            }

            public void setSiteAdmin(boolean siteAdmin) {
                this.siteAdmin = siteAdmin;
            }
        }

        public static class RepoBean implements Serializable{
            /**
             * id : 23360997
             * name : BuildInfrastructure
             * full_name : quidryan/BuildInfrastructure
             * owner : {"login":"quidryan","id":360255,"avatar_url":"https://avatars0.githubusercontent.com/u/360255?v=4","gravatar_id":"","url":"https://api.github.com/users/quidryan","html_url":"https://github.com/quidryan","followers_url":"https://api.github.com/users/quidryan/followers","following_url":"https://api.github.com/users/quidryan/following{/other_user}","gists_url":"https://api.github.com/users/quidryan/gists{/gist_id}","starred_url":"https://api.github.com/users/quidryan/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/quidryan/subscriptions","organizations_url":"https://api.github.com/users/quidryan/orgs","repos_url":"https://api.github.com/users/quidryan/repos","events_url":"https://api.github.com/users/quidryan/events{/privacy}","received_events_url":"https://api.github.com/users/quidryan/received_events","type":"User","site_admin":false}
             * private : false
             * html_url : https://github.com/quidryan/BuildInfrastructure
             * description : Test project for the new build system.
             * fork : true
             * url : https://api.github.com/repos/quidryan/BuildInfrastructure
             * forks_url : https://api.github.com/repos/quidryan/BuildInfrastructure/forks
             * keys_url : https://api.github.com/repos/quidryan/BuildInfrastructure/keys{/key_id}
             * collaborators_url : https://api.github.com/repos/quidryan/BuildInfrastructure/collaborators{/collaborator}
             * teams_url : https://api.github.com/repos/quidryan/BuildInfrastructure/teams
             * hooks_url : https://api.github.com/repos/quidryan/BuildInfrastructure/hooks
             * issue_events_url : https://api.github.com/repos/quidryan/BuildInfrastructure/issues/events{/number}
             * events_url : https://api.github.com/repos/quidryan/BuildInfrastructure/events
             * assignees_url : https://api.github.com/repos/quidryan/BuildInfrastructure/assignees{/user}
             * branches_url : https://api.github.com/repos/quidryan/BuildInfrastructure/branches{/branch}
             * tags_url : https://api.github.com/repos/quidryan/BuildInfrastructure/tags
             * blobs_url : https://api.github.com/repos/quidryan/BuildInfrastructure/git/blobs{/sha}
             * git_tags_url : https://api.github.com/repos/quidryan/BuildInfrastructure/git/tags{/sha}
             * git_refs_url : https://api.github.com/repos/quidryan/BuildInfrastructure/git/refs{/sha}
             * trees_url : https://api.github.com/repos/quidryan/BuildInfrastructure/git/trees{/sha}
             * statuses_url : https://api.github.com/repos/quidryan/BuildInfrastructure/statuses/{sha}
             * languages_url : https://api.github.com/repos/quidryan/BuildInfrastructure/languages
             * stargazers_url : https://api.github.com/repos/quidryan/BuildInfrastructure/stargazers
             * contributors_url : https://api.github.com/repos/quidryan/BuildInfrastructure/contributors
             * subscribers_url : https://api.github.com/repos/quidryan/BuildInfrastructure/subscribers
             * subscription_url : https://api.github.com/repos/quidryan/BuildInfrastructure/subscription
             * commits_url : https://api.github.com/repos/quidryan/BuildInfrastructure/commits{/sha}
             * git_commits_url : https://api.github.com/repos/quidryan/BuildInfrastructure/git/commits{/sha}
             * comments_url : https://api.github.com/repos/quidryan/BuildInfrastructure/comments{/number}
             * issue_comment_url : https://api.github.com/repos/quidryan/BuildInfrastructure/issues/comments{/number}
             * contents_url : https://api.github.com/repos/quidryan/BuildInfrastructure/contents/{+path}
             * compare_url : https://api.github.com/repos/quidryan/BuildInfrastructure/compare/{base}...{head}
             * merges_url : https://api.github.com/repos/quidryan/BuildInfrastructure/merges
             * archive_url : https://api.github.com/repos/quidryan/BuildInfrastructure/{archive_format}{/ref}
             * downloads_url : https://api.github.com/repos/quidryan/BuildInfrastructure/downloads
             * issues_url : https://api.github.com/repos/quidryan/BuildInfrastructure/issues{/number}
             * pulls_url : https://api.github.com/repos/quidryan/BuildInfrastructure/pulls{/number}
             * milestones_url : https://api.github.com/repos/quidryan/BuildInfrastructure/milestones{/number}
             * notifications_url : https://api.github.com/repos/quidryan/BuildInfrastructure/notifications{?since,all,participating}
             * labels_url : https://api.github.com/repos/quidryan/BuildInfrastructure/labels{/name}
             * releases_url : https://api.github.com/repos/quidryan/BuildInfrastructure/releases{/id}
             * deployments_url : https://api.github.com/repos/quidryan/BuildInfrastructure/deployments
             * created_at : 2014-08-26T17:46:20Z
             * updated_at : 2014-08-20T20:03:08Z
             * pushed_at : 2014-08-26T18:13:56Z
             * git_url : git://github.com/quidryan/BuildInfrastructure.git
             * ssh_url : git@github.com:quidryan/BuildInfrastructure.git
             * clone_url : https://github.com/quidryan/BuildInfrastructure.git
             * svn_url : https://github.com/quidryan/BuildInfrastructure
             * homepage : null
             * size : 145
             * stargazers_count : 0
             * watchers_count : 0
             * language : null
             * has_issues : false
             * has_projects : true
             * has_downloads : true
             * has_wiki : false
             * has_pages : false
             * forks_count : 0
             * mirror_url : null
             * archived : false
             * open_issues_count : 0
             * forks : 0
             * open_issues : 0
             * watchers : 0
             * default_branch : master
             */

            @SerializedName("id")
            private int id;
            @SerializedName("name")
            private String name;
            @SerializedName("full_name")
            private String fullName;
            @SerializedName("owner")
            private OwnerBean owner;
            @SerializedName("private")
            private boolean privateX;
            @SerializedName("html_url")
            private String htmlUrl;
            @SerializedName("description")
            private String description;
            @SerializedName("fork")
            private boolean fork;
            @SerializedName("url")
            private String url;
            @SerializedName("forks_url")
            private String forksUrl;
            @SerializedName("keys_url")
            private String keysUrl;
            @SerializedName("collaborators_url")
            private String collaboratorsUrl;
            @SerializedName("teams_url")
            private String teamsUrl;
            @SerializedName("hooks_url")
            private String hooksUrl;
            @SerializedName("issue_events_url")
            private String issueEventsUrl;
            @SerializedName("events_url")
            private String eventsUrl;
            @SerializedName("assignees_url")
            private String assigneesUrl;
            @SerializedName("branches_url")
            private String branchesUrl;
            @SerializedName("tags_url")
            private String tagsUrl;
            @SerializedName("blobs_url")
            private String blobsUrl;
            @SerializedName("git_tags_url")
            private String gitTagsUrl;
            @SerializedName("git_refs_url")
            private String gitRefsUrl;
            @SerializedName("trees_url")
            private String treesUrl;
            @SerializedName("statuses_url")
            private String statusesUrl;
            @SerializedName("languages_url")
            private String languagesUrl;
            @SerializedName("stargazers_url")
            private String stargazersUrl;
            @SerializedName("contributors_url")
            private String contributorsUrl;
            @SerializedName("subscribers_url")
            private String subscribersUrl;
            @SerializedName("subscription_url")
            private String subscriptionUrl;
            @SerializedName("commits_url")
            private String commitsUrl;
            @SerializedName("git_commits_url")
            private String gitCommitsUrl;
            @SerializedName("comments_url")
            private String commentsUrl;
            @SerializedName("issue_comment_url")
            private String issueCommentUrl;
            @SerializedName("contents_url")
            private String contentsUrl;
            @SerializedName("compare_url")
            private String compareUrl;
            @SerializedName("merges_url")
            private String mergesUrl;
            @SerializedName("archive_url")
            private String archiveUrl;
            @SerializedName("downloads_url")
            private String downloadsUrl;
            @SerializedName("issues_url")
            private String issuesUrl;
            @SerializedName("pulls_url")
            private String pullsUrl;
            @SerializedName("milestones_url")
            private String milestonesUrl;
            @SerializedName("notifications_url")
            private String notificationsUrl;
            @SerializedName("labels_url")
            private String labelsUrl;
            @SerializedName("releases_url")
            private String releasesUrl;
            @SerializedName("deployments_url")
            private String deploymentsUrl;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("pushed_at")
            private String pushedAt;
            @SerializedName("git_url")
            private String gitUrl;
            @SerializedName("ssh_url")
            private String sshUrl;
            @SerializedName("clone_url")
            private String cloneUrl;
            @SerializedName("svn_url")
            private String svnUrl;
            @SerializedName("homepage")
            private Object homepage;
            @SerializedName("size")
            private int size;
            @SerializedName("stargazers_count")
            private int stargazersCount;
            @SerializedName("watchers_count")
            private int watchersCount;
            @SerializedName("language")
            private Object language;
            @SerializedName("has_issues")
            private boolean hasIssues;
            @SerializedName("has_projects")
            private boolean hasProjects;
            @SerializedName("has_downloads")
            private boolean hasDownloads;
            @SerializedName("has_wiki")
            private boolean hasWiki;
            @SerializedName("has_pages")
            private boolean hasPages;
            @SerializedName("forks_count")
            private int forksCount;
            @SerializedName("mirror_url")
            private Object mirrorUrl;
            @SerializedName("archived")
            private boolean archived;
            @SerializedName("open_issues_count")
            private int openIssuesCount;
            @SerializedName("forks")
            private int forks;
            @SerializedName("open_issues")
            private int openIssues;
            @SerializedName("watchers")
            private int watchers;
            @SerializedName("default_branch")
            private String defaultBranch;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getFullName() {
                return fullName;
            }

            public void setFullName(String fullName) {
                this.fullName = fullName;
            }

            public OwnerBean getOwner() {
                return owner;
            }

            public void setOwner(OwnerBean owner) {
                this.owner = owner;
            }

            public boolean isPrivateX() {
                return privateX;
            }

            public void setPrivateX(boolean privateX) {
                this.privateX = privateX;
            }

            public String getHtmlUrl() {
                return htmlUrl;
            }

            public void setHtmlUrl(String htmlUrl) {
                this.htmlUrl = htmlUrl;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public boolean isFork() {
                return fork;
            }

            public void setFork(boolean fork) {
                this.fork = fork;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getForksUrl() {
                return forksUrl;
            }

            public void setForksUrl(String forksUrl) {
                this.forksUrl = forksUrl;
            }

            public String getKeysUrl() {
                return keysUrl;
            }

            public void setKeysUrl(String keysUrl) {
                this.keysUrl = keysUrl;
            }

            public String getCollaboratorsUrl() {
                return collaboratorsUrl;
            }

            public void setCollaboratorsUrl(String collaboratorsUrl) {
                this.collaboratorsUrl = collaboratorsUrl;
            }

            public String getTeamsUrl() {
                return teamsUrl;
            }

            public void setTeamsUrl(String teamsUrl) {
                this.teamsUrl = teamsUrl;
            }

            public String getHooksUrl() {
                return hooksUrl;
            }

            public void setHooksUrl(String hooksUrl) {
                this.hooksUrl = hooksUrl;
            }

            public String getIssueEventsUrl() {
                return issueEventsUrl;
            }

            public void setIssueEventsUrl(String issueEventsUrl) {
                this.issueEventsUrl = issueEventsUrl;
            }

            public String getEventsUrl() {
                return eventsUrl;
            }

            public void setEventsUrl(String eventsUrl) {
                this.eventsUrl = eventsUrl;
            }

            public String getAssigneesUrl() {
                return assigneesUrl;
            }

            public void setAssigneesUrl(String assigneesUrl) {
                this.assigneesUrl = assigneesUrl;
            }

            public String getBranchesUrl() {
                return branchesUrl;
            }

            public void setBranchesUrl(String branchesUrl) {
                this.branchesUrl = branchesUrl;
            }

            public String getTagsUrl() {
                return tagsUrl;
            }

            public void setTagsUrl(String tagsUrl) {
                this.tagsUrl = tagsUrl;
            }

            public String getBlobsUrl() {
                return blobsUrl;
            }

            public void setBlobsUrl(String blobsUrl) {
                this.blobsUrl = blobsUrl;
            }

            public String getGitTagsUrl() {
                return gitTagsUrl;
            }

            public void setGitTagsUrl(String gitTagsUrl) {
                this.gitTagsUrl = gitTagsUrl;
            }

            public String getGitRefsUrl() {
                return gitRefsUrl;
            }

            public void setGitRefsUrl(String gitRefsUrl) {
                this.gitRefsUrl = gitRefsUrl;
            }

            public String getTreesUrl() {
                return treesUrl;
            }

            public void setTreesUrl(String treesUrl) {
                this.treesUrl = treesUrl;
            }

            public String getStatusesUrl() {
                return statusesUrl;
            }

            public void setStatusesUrl(String statusesUrl) {
                this.statusesUrl = statusesUrl;
            }

            public String getLanguagesUrl() {
                return languagesUrl;
            }

            public void setLanguagesUrl(String languagesUrl) {
                this.languagesUrl = languagesUrl;
            }

            public String getStargazersUrl() {
                return stargazersUrl;
            }

            public void setStargazersUrl(String stargazersUrl) {
                this.stargazersUrl = stargazersUrl;
            }

            public String getContributorsUrl() {
                return contributorsUrl;
            }

            public void setContributorsUrl(String contributorsUrl) {
                this.contributorsUrl = contributorsUrl;
            }

            public String getSubscribersUrl() {
                return subscribersUrl;
            }

            public void setSubscribersUrl(String subscribersUrl) {
                this.subscribersUrl = subscribersUrl;
            }

            public String getSubscriptionUrl() {
                return subscriptionUrl;
            }

            public void setSubscriptionUrl(String subscriptionUrl) {
                this.subscriptionUrl = subscriptionUrl;
            }

            public String getCommitsUrl() {
                return commitsUrl;
            }

            public void setCommitsUrl(String commitsUrl) {
                this.commitsUrl = commitsUrl;
            }

            public String getGitCommitsUrl() {
                return gitCommitsUrl;
            }

            public void setGitCommitsUrl(String gitCommitsUrl) {
                this.gitCommitsUrl = gitCommitsUrl;
            }

            public String getCommentsUrl() {
                return commentsUrl;
            }

            public void setCommentsUrl(String commentsUrl) {
                this.commentsUrl = commentsUrl;
            }

            public String getIssueCommentUrl() {
                return issueCommentUrl;
            }

            public void setIssueCommentUrl(String issueCommentUrl) {
                this.issueCommentUrl = issueCommentUrl;
            }

            public String getContentsUrl() {
                return contentsUrl;
            }

            public void setContentsUrl(String contentsUrl) {
                this.contentsUrl = contentsUrl;
            }

            public String getCompareUrl() {
                return compareUrl;
            }

            public void setCompareUrl(String compareUrl) {
                this.compareUrl = compareUrl;
            }

            public String getMergesUrl() {
                return mergesUrl;
            }

            public void setMergesUrl(String mergesUrl) {
                this.mergesUrl = mergesUrl;
            }

            public String getArchiveUrl() {
                return archiveUrl;
            }

            public void setArchiveUrl(String archiveUrl) {
                this.archiveUrl = archiveUrl;
            }

            public String getDownloadsUrl() {
                return downloadsUrl;
            }

            public void setDownloadsUrl(String downloadsUrl) {
                this.downloadsUrl = downloadsUrl;
            }

            public String getIssuesUrl() {
                return issuesUrl;
            }

            public void setIssuesUrl(String issuesUrl) {
                this.issuesUrl = issuesUrl;
            }

            public String getPullsUrl() {
                return pullsUrl;
            }

            public void setPullsUrl(String pullsUrl) {
                this.pullsUrl = pullsUrl;
            }

            public String getMilestonesUrl() {
                return milestonesUrl;
            }

            public void setMilestonesUrl(String milestonesUrl) {
                this.milestonesUrl = milestonesUrl;
            }

            public String getNotificationsUrl() {
                return notificationsUrl;
            }

            public void setNotificationsUrl(String notificationsUrl) {
                this.notificationsUrl = notificationsUrl;
            }

            public String getLabelsUrl() {
                return labelsUrl;
            }

            public void setLabelsUrl(String labelsUrl) {
                this.labelsUrl = labelsUrl;
            }

            public String getReleasesUrl() {
                return releasesUrl;
            }

            public void setReleasesUrl(String releasesUrl) {
                this.releasesUrl = releasesUrl;
            }

            public String getDeploymentsUrl() {
                return deploymentsUrl;
            }

            public void setDeploymentsUrl(String deploymentsUrl) {
                this.deploymentsUrl = deploymentsUrl;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getPushedAt() {
                return pushedAt;
            }

            public void setPushedAt(String pushedAt) {
                this.pushedAt = pushedAt;
            }

            public String getGitUrl() {
                return gitUrl;
            }

            public void setGitUrl(String gitUrl) {
                this.gitUrl = gitUrl;
            }

            public String getSshUrl() {
                return sshUrl;
            }

            public void setSshUrl(String sshUrl) {
                this.sshUrl = sshUrl;
            }

            public String getCloneUrl() {
                return cloneUrl;
            }

            public void setCloneUrl(String cloneUrl) {
                this.cloneUrl = cloneUrl;
            }

            public String getSvnUrl() {
                return svnUrl;
            }

            public void setSvnUrl(String svnUrl) {
                this.svnUrl = svnUrl;
            }

            public Object getHomepage() {
                return homepage;
            }

            public void setHomepage(Object homepage) {
                this.homepage = homepage;
            }

            public int getSize() {
                return size;
            }

            public void setSize(int size) {
                this.size = size;
            }

            public int getStargazersCount() {
                return stargazersCount;
            }

            public void setStargazersCount(int stargazersCount) {
                this.stargazersCount = stargazersCount;
            }

            public int getWatchersCount() {
                return watchersCount;
            }

            public void setWatchersCount(int watchersCount) {
                this.watchersCount = watchersCount;
            }

            public Object getLanguage() {
                return language;
            }

            public void setLanguage(Object language) {
                this.language = language;
            }

            public boolean isHasIssues() {
                return hasIssues;
            }

            public void setHasIssues(boolean hasIssues) {
                this.hasIssues = hasIssues;
            }

            public boolean isHasProjects() {
                return hasProjects;
            }

            public void setHasProjects(boolean hasProjects) {
                this.hasProjects = hasProjects;
            }

            public boolean isHasDownloads() {
                return hasDownloads;
            }

            public void setHasDownloads(boolean hasDownloads) {
                this.hasDownloads = hasDownloads;
            }

            public boolean isHasWiki() {
                return hasWiki;
            }

            public void setHasWiki(boolean hasWiki) {
                this.hasWiki = hasWiki;
            }

            public boolean isHasPages() {
                return hasPages;
            }

            public void setHasPages(boolean hasPages) {
                this.hasPages = hasPages;
            }

            public int getForksCount() {
                return forksCount;
            }

            public void setForksCount(int forksCount) {
                this.forksCount = forksCount;
            }

            public Object getMirrorUrl() {
                return mirrorUrl;
            }

            public void setMirrorUrl(Object mirrorUrl) {
                this.mirrorUrl = mirrorUrl;
            }

            public boolean isArchived() {
                return archived;
            }

            public void setArchived(boolean archived) {
                this.archived = archived;
            }

            public int getOpenIssuesCount() {
                return openIssuesCount;
            }

            public void setOpenIssuesCount(int openIssuesCount) {
                this.openIssuesCount = openIssuesCount;
            }

            public int getForks() {
                return forks;
            }

            public void setForks(int forks) {
                this.forks = forks;
            }

            public int getOpenIssues() {
                return openIssues;
            }

            public void setOpenIssues(int openIssues) {
                this.openIssues = openIssues;
            }

            public int getWatchers() {
                return watchers;
            }

            public void setWatchers(int watchers) {
                this.watchers = watchers;
            }

            public String getDefaultBranch() {
                return defaultBranch;
            }

            public void setDefaultBranch(String defaultBranch) {
                this.defaultBranch = defaultBranch;
            }

            public static class OwnerBean implements Serializable{
                /**
                 * login : quidryan
                 * id : 360255
                 * avatar_url : https://avatars0.githubusercontent.com/u/360255?v=4
                 * gravatar_id :
                 * url : https://api.github.com/users/quidryan
                 * html_url : https://github.com/quidryan
                 * followers_url : https://api.github.com/users/quidryan/followers
                 * following_url : https://api.github.com/users/quidryan/following{/other_user}
                 * gists_url : https://api.github.com/users/quidryan/gists{/gist_id}
                 * starred_url : https://api.github.com/users/quidryan/starred{/owner}{/repo}
                 * subscriptions_url : https://api.github.com/users/quidryan/subscriptions
                 * organizations_url : https://api.github.com/users/quidryan/orgs
                 * repos_url : https://api.github.com/users/quidryan/repos
                 * events_url : https://api.github.com/users/quidryan/events{/privacy}
                 * received_events_url : https://api.github.com/users/quidryan/received_events
                 * type : User
                 * site_admin : false
                 */

                @SerializedName("login")
                private String login;
                @SerializedName("id")
                private int id;
                @SerializedName("avatar_url")
                private String avatarUrl;
                @SerializedName("gravatar_id")
                private String gravatarId;
                @SerializedName("url")
                private String url;
                @SerializedName("html_url")
                private String htmlUrl;
                @SerializedName("followers_url")
                private String followersUrl;
                @SerializedName("following_url")
                private String followingUrl;
                @SerializedName("gists_url")
                private String gistsUrl;
                @SerializedName("starred_url")
                private String starredUrl;
                @SerializedName("subscriptions_url")
                private String subscriptionsUrl;
                @SerializedName("organizations_url")
                private String organizationsUrl;
                @SerializedName("repos_url")
                private String reposUrl;
                @SerializedName("events_url")
                private String eventsUrl;
                @SerializedName("received_events_url")
                private String receivedEventsUrl;
                @SerializedName("type")
                private String type;
                @SerializedName("site_admin")
                private boolean siteAdmin;

                public String getLogin() {
                    return login;
                }

                public void setLogin(String login) {
                    this.login = login;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getAvatarUrl() {
                    return avatarUrl;
                }

                public void setAvatarUrl(String avatarUrl) {
                    this.avatarUrl = avatarUrl;
                }

                public String getGravatarId() {
                    return gravatarId;
                }

                public void setGravatarId(String gravatarId) {
                    this.gravatarId = gravatarId;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getHtmlUrl() {
                    return htmlUrl;
                }

                public void setHtmlUrl(String htmlUrl) {
                    this.htmlUrl = htmlUrl;
                }

                public String getFollowersUrl() {
                    return followersUrl;
                }

                public void setFollowersUrl(String followersUrl) {
                    this.followersUrl = followersUrl;
                }

                public String getFollowingUrl() {
                    return followingUrl;
                }

                public void setFollowingUrl(String followingUrl) {
                    this.followingUrl = followingUrl;
                }

                public String getGistsUrl() {
                    return gistsUrl;
                }

                public void setGistsUrl(String gistsUrl) {
                    this.gistsUrl = gistsUrl;
                }

                public String getStarredUrl() {
                    return starredUrl;
                }

                public void setStarredUrl(String starredUrl) {
                    this.starredUrl = starredUrl;
                }

                public String getSubscriptionsUrl() {
                    return subscriptionsUrl;
                }

                public void setSubscriptionsUrl(String subscriptionsUrl) {
                    this.subscriptionsUrl = subscriptionsUrl;
                }

                public String getOrganizationsUrl() {
                    return organizationsUrl;
                }

                public void setOrganizationsUrl(String organizationsUrl) {
                    this.organizationsUrl = organizationsUrl;
                }

                public String getReposUrl() {
                    return reposUrl;
                }

                public void setReposUrl(String reposUrl) {
                    this.reposUrl = reposUrl;
                }

                public String getEventsUrl() {
                    return eventsUrl;
                }

                public void setEventsUrl(String eventsUrl) {
                    this.eventsUrl = eventsUrl;
                }

                public String getReceivedEventsUrl() {
                    return receivedEventsUrl;
                }

                public void setReceivedEventsUrl(String receivedEventsUrl) {
                    this.receivedEventsUrl = receivedEventsUrl;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public boolean isSiteAdmin() {
                    return siteAdmin;
                }

                public void setSiteAdmin(boolean siteAdmin) {
                    this.siteAdmin = siteAdmin;
                }
            }
        }
    }

    public static class BaseBean implements Serializable{
        /**
         * label : ReactiveX:master
         * ref : master
         * sha : 39e6f7d128879828137e310e3811559efa8fe8ba
         * user : {"login":"ReactiveX","id":6407041,"avatar_url":"https://avatars1.githubusercontent.com/u/6407041?v=4","gravatar_id":"","url":"https://api.github.com/users/ReactiveX","html_url":"https://github.com/ReactiveX","followers_url":"https://api.github.com/users/ReactiveX/followers","following_url":"https://api.github.com/users/ReactiveX/following{/other_user}","gists_url":"https://api.github.com/users/ReactiveX/gists{/gist_id}","starred_url":"https://api.github.com/users/ReactiveX/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ReactiveX/subscriptions","organizations_url":"https://api.github.com/users/ReactiveX/orgs","repos_url":"https://api.github.com/users/ReactiveX/repos","events_url":"https://api.github.com/users/ReactiveX/events{/privacy}","received_events_url":"https://api.github.com/users/ReactiveX/received_events","type":"Organization","site_admin":false}
         * repo : {"id":23123948,"name":"BuildInfrastructure","full_name":"ReactiveX/BuildInfrastructure","owner":{"login":"ReactiveX","id":6407041,"avatar_url":"https://avatars1.githubusercontent.com/u/6407041?v=4","gravatar_id":"","url":"https://api.github.com/users/ReactiveX","html_url":"https://github.com/ReactiveX","followers_url":"https://api.github.com/users/ReactiveX/followers","following_url":"https://api.github.com/users/ReactiveX/following{/other_user}","gists_url":"https://api.github.com/users/ReactiveX/gists{/gist_id}","starred_url":"https://api.github.com/users/ReactiveX/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ReactiveX/subscriptions","organizations_url":"https://api.github.com/users/ReactiveX/orgs","repos_url":"https://api.github.com/users/ReactiveX/repos","events_url":"https://api.github.com/users/ReactiveX/events{/privacy}","received_events_url":"https://api.github.com/users/ReactiveX/received_events","type":"Organization","site_admin":false},"private":false,"html_url":"https://github.com/ReactiveX/BuildInfrastructure","description":"Test project for the new build system.","fork":false,"url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure","forks_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/forks","keys_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/keys{/key_id}","collaborators_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/collaborators{/collaborator}","teams_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/teams","hooks_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/hooks","issue_events_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/events{/number}","events_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/events","assignees_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/assignees{/user}","branches_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/branches{/branch}","tags_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/tags","blobs_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/blobs{/sha}","git_tags_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/tags{/sha}","git_refs_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/refs{/sha}","trees_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/trees{/sha}","statuses_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/statuses/{sha}","languages_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/languages","stargazers_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/stargazers","contributors_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/contributors","subscribers_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/subscribers","subscription_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/subscription","commits_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/commits{/sha}","git_commits_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/commits{/sha}","comments_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/comments{/number}","issue_comment_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/comments{/number}","contents_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/contents/{+path}","compare_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/compare/{base}...{head}","merges_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/merges","archive_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/{archive_format}{/ref}","downloads_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/downloads","issues_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues{/number}","pulls_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls{/number}","milestones_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/milestones{/number}","notifications_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/notifications{?since,all,participating}","labels_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/labels{/name}","releases_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/releases{/id}","deployments_url":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/deployments","created_at":"2014-08-19T20:03:00Z","updated_at":"2017-11-07T09:25:51Z","pushed_at":"2015-01-19T21:55:02Z","git_url":"git://github.com/ReactiveX/BuildInfrastructure.git","ssh_url":"git@github.com:ReactiveX/BuildInfrastructure.git","clone_url":"https://github.com/ReactiveX/BuildInfrastructure.git","svn_url":"https://github.com/ReactiveX/BuildInfrastructure","homepage":null,"size":682,"stargazers_count":1,"watchers_count":1,"language":"Groovy","has_issues":true,"has_projects":true,"has_downloads":true,"has_wiki":false,"has_pages":false,"forks_count":7,"mirror_url":null,"archived":false,"open_issues_count":0,"forks":7,"open_issues":0,"watchers":1,"default_branch":"master"}
         */

        @SerializedName("label")
        private String label;
        @SerializedName("ref")
        private String ref;
        @SerializedName("sha")
        private String sha;
        @SerializedName("user")
        private UserBeanXX user;
        @SerializedName("repo")
        private RepoBeanX repo;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getRef() {
            return ref;
        }

        public void setRef(String ref) {
            this.ref = ref;
        }

        public String getSha() {
            return sha;
        }

        public void setSha(String sha) {
            this.sha = sha;
        }

        public UserBeanXX getUser() {
            return user;
        }

        public void setUser(UserBeanXX user) {
            this.user = user;
        }

        public RepoBeanX getRepo() {
            return repo;
        }

        public void setRepo(RepoBeanX repo) {
            this.repo = repo;
        }

        public static class UserBeanXX implements Serializable{
            /**
             * login : ReactiveX
             * id : 6407041
             * avatar_url : https://avatars1.githubusercontent.com/u/6407041?v=4
             * gravatar_id :
             * url : https://api.github.com/users/ReactiveX
             * html_url : https://github.com/ReactiveX
             * followers_url : https://api.github.com/users/ReactiveX/followers
             * following_url : https://api.github.com/users/ReactiveX/following{/other_user}
             * gists_url : https://api.github.com/users/ReactiveX/gists{/gist_id}
             * starred_url : https://api.github.com/users/ReactiveX/starred{/owner}{/repo}
             * subscriptions_url : https://api.github.com/users/ReactiveX/subscriptions
             * organizations_url : https://api.github.com/users/ReactiveX/orgs
             * repos_url : https://api.github.com/users/ReactiveX/repos
             * events_url : https://api.github.com/users/ReactiveX/events{/privacy}
             * received_events_url : https://api.github.com/users/ReactiveX/received_events
             * type : Organization
             * site_admin : false
             */

            @SerializedName("login")
            private String login;
            @SerializedName("id")
            private int id;
            @SerializedName("avatar_url")
            private String avatarUrl;
            @SerializedName("gravatar_id")
            private String gravatarId;
            @SerializedName("url")
            private String url;
            @SerializedName("html_url")
            private String htmlUrl;
            @SerializedName("followers_url")
            private String followersUrl;
            @SerializedName("following_url")
            private String followingUrl;
            @SerializedName("gists_url")
            private String gistsUrl;
            @SerializedName("starred_url")
            private String starredUrl;
            @SerializedName("subscriptions_url")
            private String subscriptionsUrl;
            @SerializedName("organizations_url")
            private String organizationsUrl;
            @SerializedName("repos_url")
            private String reposUrl;
            @SerializedName("events_url")
            private String eventsUrl;
            @SerializedName("received_events_url")
            private String receivedEventsUrl;
            @SerializedName("type")
            private String type;
            @SerializedName("site_admin")
            private boolean siteAdmin;

            public String getLogin() {
                return login;
            }

            public void setLogin(String login) {
                this.login = login;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getAvatarUrl() {
                return avatarUrl;
            }

            public void setAvatarUrl(String avatarUrl) {
                this.avatarUrl = avatarUrl;
            }

            public String getGravatarId() {
                return gravatarId;
            }

            public void setGravatarId(String gravatarId) {
                this.gravatarId = gravatarId;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getHtmlUrl() {
                return htmlUrl;
            }

            public void setHtmlUrl(String htmlUrl) {
                this.htmlUrl = htmlUrl;
            }

            public String getFollowersUrl() {
                return followersUrl;
            }

            public void setFollowersUrl(String followersUrl) {
                this.followersUrl = followersUrl;
            }

            public String getFollowingUrl() {
                return followingUrl;
            }

            public void setFollowingUrl(String followingUrl) {
                this.followingUrl = followingUrl;
            }

            public String getGistsUrl() {
                return gistsUrl;
            }

            public void setGistsUrl(String gistsUrl) {
                this.gistsUrl = gistsUrl;
            }

            public String getStarredUrl() {
                return starredUrl;
            }

            public void setStarredUrl(String starredUrl) {
                this.starredUrl = starredUrl;
            }

            public String getSubscriptionsUrl() {
                return subscriptionsUrl;
            }

            public void setSubscriptionsUrl(String subscriptionsUrl) {
                this.subscriptionsUrl = subscriptionsUrl;
            }

            public String getOrganizationsUrl() {
                return organizationsUrl;
            }

            public void setOrganizationsUrl(String organizationsUrl) {
                this.organizationsUrl = organizationsUrl;
            }

            public String getReposUrl() {
                return reposUrl;
            }

            public void setReposUrl(String reposUrl) {
                this.reposUrl = reposUrl;
            }

            public String getEventsUrl() {
                return eventsUrl;
            }

            public void setEventsUrl(String eventsUrl) {
                this.eventsUrl = eventsUrl;
            }

            public String getReceivedEventsUrl() {
                return receivedEventsUrl;
            }

            public void setReceivedEventsUrl(String receivedEventsUrl) {
                this.receivedEventsUrl = receivedEventsUrl;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public boolean isSiteAdmin() {
                return siteAdmin;
            }

            public void setSiteAdmin(boolean siteAdmin) {
                this.siteAdmin = siteAdmin;
            }
        }

        public static class RepoBeanX implements Serializable{
            /**
             * id : 23123948
             * name : BuildInfrastructure
             * full_name : ReactiveX/BuildInfrastructure
             * owner : {"login":"ReactiveX","id":6407041,"avatar_url":"https://avatars1.githubusercontent.com/u/6407041?v=4","gravatar_id":"","url":"https://api.github.com/users/ReactiveX","html_url":"https://github.com/ReactiveX","followers_url":"https://api.github.com/users/ReactiveX/followers","following_url":"https://api.github.com/users/ReactiveX/following{/other_user}","gists_url":"https://api.github.com/users/ReactiveX/gists{/gist_id}","starred_url":"https://api.github.com/users/ReactiveX/starred{/owner}{/repo}","subscriptions_url":"https://api.github.com/users/ReactiveX/subscriptions","organizations_url":"https://api.github.com/users/ReactiveX/orgs","repos_url":"https://api.github.com/users/ReactiveX/repos","events_url":"https://api.github.com/users/ReactiveX/events{/privacy}","received_events_url":"https://api.github.com/users/ReactiveX/received_events","type":"Organization","site_admin":false}
             * private : false
             * html_url : https://github.com/ReactiveX/BuildInfrastructure
             * description : Test project for the new build system.
             * fork : false
             * url : https://api.github.com/repos/ReactiveX/BuildInfrastructure
             * forks_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/forks
             * keys_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/keys{/key_id}
             * collaborators_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/collaborators{/collaborator}
             * teams_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/teams
             * hooks_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/hooks
             * issue_events_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/events{/number}
             * events_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/events
             * assignees_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/assignees{/user}
             * branches_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/branches{/branch}
             * tags_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/tags
             * blobs_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/blobs{/sha}
             * git_tags_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/tags{/sha}
             * git_refs_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/refs{/sha}
             * trees_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/trees{/sha}
             * statuses_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/statuses/{sha}
             * languages_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/languages
             * stargazers_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/stargazers
             * contributors_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/contributors
             * subscribers_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/subscribers
             * subscription_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/subscription
             * commits_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/commits{/sha}
             * git_commits_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/git/commits{/sha}
             * comments_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/comments{/number}
             * issue_comment_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/comments{/number}
             * contents_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/contents/{+path}
             * compare_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/compare/{base}...{head}
             * merges_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/merges
             * archive_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/{archive_format}{/ref}
             * downloads_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/downloads
             * issues_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues{/number}
             * pulls_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls{/number}
             * milestones_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/milestones{/number}
             * notifications_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/notifications{?since,all,participating}
             * labels_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/labels{/name}
             * releases_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/releases{/id}
             * deployments_url : https://api.github.com/repos/ReactiveX/BuildInfrastructure/deployments
             * created_at : 2014-08-19T20:03:00Z
             * updated_at : 2017-11-07T09:25:51Z
             * pushed_at : 2015-01-19T21:55:02Z
             * git_url : git://github.com/ReactiveX/BuildInfrastructure.git
             * ssh_url : git@github.com:ReactiveX/BuildInfrastructure.git
             * clone_url : https://github.com/ReactiveX/BuildInfrastructure.git
             * svn_url : https://github.com/ReactiveX/BuildInfrastructure
             * homepage : null
             * size : 682
             * stargazers_count : 1
             * watchers_count : 1
             * language : Groovy
             * has_issues : true
             * has_projects : true
             * has_downloads : true
             * has_wiki : false
             * has_pages : false
             * forks_count : 7
             * mirror_url : null
             * archived : false
             * open_issues_count : 0
             * forks : 7
             * open_issues : 0
             * watchers : 1
             * default_branch : master
             */

            @SerializedName("id")
            private int id;
            @SerializedName("name")
            private String name;
            @SerializedName("full_name")
            private String fullName;
            @SerializedName("owner")
            private OwnerBeanX owner;
            @SerializedName("private")
            private boolean privateX;
            @SerializedName("html_url")
            private String htmlUrl;
            @SerializedName("description")
            private String description;
            @SerializedName("fork")
            private boolean fork;
            @SerializedName("url")
            private String url;
            @SerializedName("forks_url")
            private String forksUrl;
            @SerializedName("keys_url")
            private String keysUrl;
            @SerializedName("collaborators_url")
            private String collaboratorsUrl;
            @SerializedName("teams_url")
            private String teamsUrl;
            @SerializedName("hooks_url")
            private String hooksUrl;
            @SerializedName("issue_events_url")
            private String issueEventsUrl;
            @SerializedName("events_url")
            private String eventsUrl;
            @SerializedName("assignees_url")
            private String assigneesUrl;
            @SerializedName("branches_url")
            private String branchesUrl;
            @SerializedName("tags_url")
            private String tagsUrl;
            @SerializedName("blobs_url")
            private String blobsUrl;
            @SerializedName("git_tags_url")
            private String gitTagsUrl;
            @SerializedName("git_refs_url")
            private String gitRefsUrl;
            @SerializedName("trees_url")
            private String treesUrl;
            @SerializedName("statuses_url")
            private String statusesUrl;
            @SerializedName("languages_url")
            private String languagesUrl;
            @SerializedName("stargazers_url")
            private String stargazersUrl;
            @SerializedName("contributors_url")
            private String contributorsUrl;
            @SerializedName("subscribers_url")
            private String subscribersUrl;
            @SerializedName("subscription_url")
            private String subscriptionUrl;
            @SerializedName("commits_url")
            private String commitsUrl;
            @SerializedName("git_commits_url")
            private String gitCommitsUrl;
            @SerializedName("comments_url")
            private String commentsUrl;
            @SerializedName("issue_comment_url")
            private String issueCommentUrl;
            @SerializedName("contents_url")
            private String contentsUrl;
            @SerializedName("compare_url")
            private String compareUrl;
            @SerializedName("merges_url")
            private String mergesUrl;
            @SerializedName("archive_url")
            private String archiveUrl;
            @SerializedName("downloads_url")
            private String downloadsUrl;
            @SerializedName("issues_url")
            private String issuesUrl;
            @SerializedName("pulls_url")
            private String pullsUrl;
            @SerializedName("milestones_url")
            private String milestonesUrl;
            @SerializedName("notifications_url")
            private String notificationsUrl;
            @SerializedName("labels_url")
            private String labelsUrl;
            @SerializedName("releases_url")
            private String releasesUrl;
            @SerializedName("deployments_url")
            private String deploymentsUrl;
            @SerializedName("created_at")
            private String createdAt;
            @SerializedName("updated_at")
            private String updatedAt;
            @SerializedName("pushed_at")
            private String pushedAt;
            @SerializedName("git_url")
            private String gitUrl;
            @SerializedName("ssh_url")
            private String sshUrl;
            @SerializedName("clone_url")
            private String cloneUrl;
            @SerializedName("svn_url")
            private String svnUrl;
            @SerializedName("homepage")
            private Object homepage;
            @SerializedName("size")
            private int size;
            @SerializedName("stargazers_count")
            private int stargazersCount;
            @SerializedName("watchers_count")
            private int watchersCount;
            @SerializedName("language")
            private String language;
            @SerializedName("has_issues")
            private boolean hasIssues;
            @SerializedName("has_projects")
            private boolean hasProjects;
            @SerializedName("has_downloads")
            private boolean hasDownloads;
            @SerializedName("has_wiki")
            private boolean hasWiki;
            @SerializedName("has_pages")
            private boolean hasPages;
            @SerializedName("forks_count")
            private int forksCount;
            @SerializedName("mirror_url")
            private Object mirrorUrl;
            @SerializedName("archived")
            private boolean archived;
            @SerializedName("open_issues_count")
            private int openIssuesCount;
            @SerializedName("forks")
            private int forks;
            @SerializedName("open_issues")
            private int openIssues;
            @SerializedName("watchers")
            private int watchers;
            @SerializedName("default_branch")
            private String defaultBranch;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getFullName() {
                return fullName;
            }

            public void setFullName(String fullName) {
                this.fullName = fullName;
            }

            public OwnerBeanX getOwner() {
                return owner;
            }

            public void setOwner(OwnerBeanX owner) {
                this.owner = owner;
            }

            public boolean isPrivateX() {
                return privateX;
            }

            public void setPrivateX(boolean privateX) {
                this.privateX = privateX;
            }

            public String getHtmlUrl() {
                return htmlUrl;
            }

            public void setHtmlUrl(String htmlUrl) {
                this.htmlUrl = htmlUrl;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public boolean isFork() {
                return fork;
            }

            public void setFork(boolean fork) {
                this.fork = fork;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getForksUrl() {
                return forksUrl;
            }

            public void setForksUrl(String forksUrl) {
                this.forksUrl = forksUrl;
            }

            public String getKeysUrl() {
                return keysUrl;
            }

            public void setKeysUrl(String keysUrl) {
                this.keysUrl = keysUrl;
            }

            public String getCollaboratorsUrl() {
                return collaboratorsUrl;
            }

            public void setCollaboratorsUrl(String collaboratorsUrl) {
                this.collaboratorsUrl = collaboratorsUrl;
            }

            public String getTeamsUrl() {
                return teamsUrl;
            }

            public void setTeamsUrl(String teamsUrl) {
                this.teamsUrl = teamsUrl;
            }

            public String getHooksUrl() {
                return hooksUrl;
            }

            public void setHooksUrl(String hooksUrl) {
                this.hooksUrl = hooksUrl;
            }

            public String getIssueEventsUrl() {
                return issueEventsUrl;
            }

            public void setIssueEventsUrl(String issueEventsUrl) {
                this.issueEventsUrl = issueEventsUrl;
            }

            public String getEventsUrl() {
                return eventsUrl;
            }

            public void setEventsUrl(String eventsUrl) {
                this.eventsUrl = eventsUrl;
            }

            public String getAssigneesUrl() {
                return assigneesUrl;
            }

            public void setAssigneesUrl(String assigneesUrl) {
                this.assigneesUrl = assigneesUrl;
            }

            public String getBranchesUrl() {
                return branchesUrl;
            }

            public void setBranchesUrl(String branchesUrl) {
                this.branchesUrl = branchesUrl;
            }

            public String getTagsUrl() {
                return tagsUrl;
            }

            public void setTagsUrl(String tagsUrl) {
                this.tagsUrl = tagsUrl;
            }

            public String getBlobsUrl() {
                return blobsUrl;
            }

            public void setBlobsUrl(String blobsUrl) {
                this.blobsUrl = blobsUrl;
            }

            public String getGitTagsUrl() {
                return gitTagsUrl;
            }

            public void setGitTagsUrl(String gitTagsUrl) {
                this.gitTagsUrl = gitTagsUrl;
            }

            public String getGitRefsUrl() {
                return gitRefsUrl;
            }

            public void setGitRefsUrl(String gitRefsUrl) {
                this.gitRefsUrl = gitRefsUrl;
            }

            public String getTreesUrl() {
                return treesUrl;
            }

            public void setTreesUrl(String treesUrl) {
                this.treesUrl = treesUrl;
            }

            public String getStatusesUrl() {
                return statusesUrl;
            }

            public void setStatusesUrl(String statusesUrl) {
                this.statusesUrl = statusesUrl;
            }

            public String getLanguagesUrl() {
                return languagesUrl;
            }

            public void setLanguagesUrl(String languagesUrl) {
                this.languagesUrl = languagesUrl;
            }

            public String getStargazersUrl() {
                return stargazersUrl;
            }

            public void setStargazersUrl(String stargazersUrl) {
                this.stargazersUrl = stargazersUrl;
            }

            public String getContributorsUrl() {
                return contributorsUrl;
            }

            public void setContributorsUrl(String contributorsUrl) {
                this.contributorsUrl = contributorsUrl;
            }

            public String getSubscribersUrl() {
                return subscribersUrl;
            }

            public void setSubscribersUrl(String subscribersUrl) {
                this.subscribersUrl = subscribersUrl;
            }

            public String getSubscriptionUrl() {
                return subscriptionUrl;
            }

            public void setSubscriptionUrl(String subscriptionUrl) {
                this.subscriptionUrl = subscriptionUrl;
            }

            public String getCommitsUrl() {
                return commitsUrl;
            }

            public void setCommitsUrl(String commitsUrl) {
                this.commitsUrl = commitsUrl;
            }

            public String getGitCommitsUrl() {
                return gitCommitsUrl;
            }

            public void setGitCommitsUrl(String gitCommitsUrl) {
                this.gitCommitsUrl = gitCommitsUrl;
            }

            public String getCommentsUrl() {
                return commentsUrl;
            }

            public void setCommentsUrl(String commentsUrl) {
                this.commentsUrl = commentsUrl;
            }

            public String getIssueCommentUrl() {
                return issueCommentUrl;
            }

            public void setIssueCommentUrl(String issueCommentUrl) {
                this.issueCommentUrl = issueCommentUrl;
            }

            public String getContentsUrl() {
                return contentsUrl;
            }

            public void setContentsUrl(String contentsUrl) {
                this.contentsUrl = contentsUrl;
            }

            public String getCompareUrl() {
                return compareUrl;
            }

            public void setCompareUrl(String compareUrl) {
                this.compareUrl = compareUrl;
            }

            public String getMergesUrl() {
                return mergesUrl;
            }

            public void setMergesUrl(String mergesUrl) {
                this.mergesUrl = mergesUrl;
            }

            public String getArchiveUrl() {
                return archiveUrl;
            }

            public void setArchiveUrl(String archiveUrl) {
                this.archiveUrl = archiveUrl;
            }

            public String getDownloadsUrl() {
                return downloadsUrl;
            }

            public void setDownloadsUrl(String downloadsUrl) {
                this.downloadsUrl = downloadsUrl;
            }

            public String getIssuesUrl() {
                return issuesUrl;
            }

            public void setIssuesUrl(String issuesUrl) {
                this.issuesUrl = issuesUrl;
            }

            public String getPullsUrl() {
                return pullsUrl;
            }

            public void setPullsUrl(String pullsUrl) {
                this.pullsUrl = pullsUrl;
            }

            public String getMilestonesUrl() {
                return milestonesUrl;
            }

            public void setMilestonesUrl(String milestonesUrl) {
                this.milestonesUrl = milestonesUrl;
            }

            public String getNotificationsUrl() {
                return notificationsUrl;
            }

            public void setNotificationsUrl(String notificationsUrl) {
                this.notificationsUrl = notificationsUrl;
            }

            public String getLabelsUrl() {
                return labelsUrl;
            }

            public void setLabelsUrl(String labelsUrl) {
                this.labelsUrl = labelsUrl;
            }

            public String getReleasesUrl() {
                return releasesUrl;
            }

            public void setReleasesUrl(String releasesUrl) {
                this.releasesUrl = releasesUrl;
            }

            public String getDeploymentsUrl() {
                return deploymentsUrl;
            }

            public void setDeploymentsUrl(String deploymentsUrl) {
                this.deploymentsUrl = deploymentsUrl;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getPushedAt() {
                return pushedAt;
            }

            public void setPushedAt(String pushedAt) {
                this.pushedAt = pushedAt;
            }

            public String getGitUrl() {
                return gitUrl;
            }

            public void setGitUrl(String gitUrl) {
                this.gitUrl = gitUrl;
            }

            public String getSshUrl() {
                return sshUrl;
            }

            public void setSshUrl(String sshUrl) {
                this.sshUrl = sshUrl;
            }

            public String getCloneUrl() {
                return cloneUrl;
            }

            public void setCloneUrl(String cloneUrl) {
                this.cloneUrl = cloneUrl;
            }

            public String getSvnUrl() {
                return svnUrl;
            }

            public void setSvnUrl(String svnUrl) {
                this.svnUrl = svnUrl;
            }

            public Object getHomepage() {
                return homepage;
            }

            public void setHomepage(Object homepage) {
                this.homepage = homepage;
            }

            public int getSize() {
                return size;
            }

            public void setSize(int size) {
                this.size = size;
            }

            public int getStargazersCount() {
                return stargazersCount;
            }

            public void setStargazersCount(int stargazersCount) {
                this.stargazersCount = stargazersCount;
            }

            public int getWatchersCount() {
                return watchersCount;
            }

            public void setWatchersCount(int watchersCount) {
                this.watchersCount = watchersCount;
            }

            public String getLanguage() {
                return language;
            }

            public void setLanguage(String language) {
                this.language = language;
            }

            public boolean isHasIssues() {
                return hasIssues;
            }

            public void setHasIssues(boolean hasIssues) {
                this.hasIssues = hasIssues;
            }

            public boolean isHasProjects() {
                return hasProjects;
            }

            public void setHasProjects(boolean hasProjects) {
                this.hasProjects = hasProjects;
            }

            public boolean isHasDownloads() {
                return hasDownloads;
            }

            public void setHasDownloads(boolean hasDownloads) {
                this.hasDownloads = hasDownloads;
            }

            public boolean isHasWiki() {
                return hasWiki;
            }

            public void setHasWiki(boolean hasWiki) {
                this.hasWiki = hasWiki;
            }

            public boolean isHasPages() {
                return hasPages;
            }

            public void setHasPages(boolean hasPages) {
                this.hasPages = hasPages;
            }

            public int getForksCount() {
                return forksCount;
            }

            public void setForksCount(int forksCount) {
                this.forksCount = forksCount;
            }

            public Object getMirrorUrl() {
                return mirrorUrl;
            }

            public void setMirrorUrl(Object mirrorUrl) {
                this.mirrorUrl = mirrorUrl;
            }

            public boolean isArchived() {
                return archived;
            }

            public void setArchived(boolean archived) {
                this.archived = archived;
            }

            public int getOpenIssuesCount() {
                return openIssuesCount;
            }

            public void setOpenIssuesCount(int openIssuesCount) {
                this.openIssuesCount = openIssuesCount;
            }

            public int getForks() {
                return forks;
            }

            public void setForks(int forks) {
                this.forks = forks;
            }

            public int getOpenIssues() {
                return openIssues;
            }

            public void setOpenIssues(int openIssues) {
                this.openIssues = openIssues;
            }

            public int getWatchers() {
                return watchers;
            }

            public void setWatchers(int watchers) {
                this.watchers = watchers;
            }

            public String getDefaultBranch() {
                return defaultBranch;
            }

            public void setDefaultBranch(String defaultBranch) {
                this.defaultBranch = defaultBranch;
            }

            public static class OwnerBeanX implements Serializable{
                /**
                 * login : ReactiveX
                 * id : 6407041
                 * avatar_url : https://avatars1.githubusercontent.com/u/6407041?v=4
                 * gravatar_id :
                 * url : https://api.github.com/users/ReactiveX
                 * html_url : https://github.com/ReactiveX
                 * followers_url : https://api.github.com/users/ReactiveX/followers
                 * following_url : https://api.github.com/users/ReactiveX/following{/other_user}
                 * gists_url : https://api.github.com/users/ReactiveX/gists{/gist_id}
                 * starred_url : https://api.github.com/users/ReactiveX/starred{/owner}{/repo}
                 * subscriptions_url : https://api.github.com/users/ReactiveX/subscriptions
                 * organizations_url : https://api.github.com/users/ReactiveX/orgs
                 * repos_url : https://api.github.com/users/ReactiveX/repos
                 * events_url : https://api.github.com/users/ReactiveX/events{/privacy}
                 * received_events_url : https://api.github.com/users/ReactiveX/received_events
                 * type : Organization
                 * site_admin : false
                 */

                @SerializedName("login")
                private String login;
                @SerializedName("id")
                private int id;
                @SerializedName("avatar_url")
                private String avatarUrl;
                @SerializedName("gravatar_id")
                private String gravatarId;
                @SerializedName("url")
                private String url;
                @SerializedName("html_url")
                private String htmlUrl;
                @SerializedName("followers_url")
                private String followersUrl;
                @SerializedName("following_url")
                private String followingUrl;
                @SerializedName("gists_url")
                private String gistsUrl;
                @SerializedName("starred_url")
                private String starredUrl;
                @SerializedName("subscriptions_url")
                private String subscriptionsUrl;
                @SerializedName("organizations_url")
                private String organizationsUrl;
                @SerializedName("repos_url")
                private String reposUrl;
                @SerializedName("events_url")
                private String eventsUrl;
                @SerializedName("received_events_url")
                private String receivedEventsUrl;
                @SerializedName("type")
                private String type;
                @SerializedName("site_admin")
                private boolean siteAdmin;

                public String getLogin() {
                    return login;
                }

                public void setLogin(String login) {
                    this.login = login;
                }

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getAvatarUrl() {
                    return avatarUrl;
                }

                public void setAvatarUrl(String avatarUrl) {
                    this.avatarUrl = avatarUrl;
                }

                public String getGravatarId() {
                    return gravatarId;
                }

                public void setGravatarId(String gravatarId) {
                    this.gravatarId = gravatarId;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getHtmlUrl() {
                    return htmlUrl;
                }

                public void setHtmlUrl(String htmlUrl) {
                    this.htmlUrl = htmlUrl;
                }

                public String getFollowersUrl() {
                    return followersUrl;
                }

                public void setFollowersUrl(String followersUrl) {
                    this.followersUrl = followersUrl;
                }

                public String getFollowingUrl() {
                    return followingUrl;
                }

                public void setFollowingUrl(String followingUrl) {
                    this.followingUrl = followingUrl;
                }

                public String getGistsUrl() {
                    return gistsUrl;
                }

                public void setGistsUrl(String gistsUrl) {
                    this.gistsUrl = gistsUrl;
                }

                public String getStarredUrl() {
                    return starredUrl;
                }

                public void setStarredUrl(String starredUrl) {
                    this.starredUrl = starredUrl;
                }

                public String getSubscriptionsUrl() {
                    return subscriptionsUrl;
                }

                public void setSubscriptionsUrl(String subscriptionsUrl) {
                    this.subscriptionsUrl = subscriptionsUrl;
                }

                public String getOrganizationsUrl() {
                    return organizationsUrl;
                }

                public void setOrganizationsUrl(String organizationsUrl) {
                    this.organizationsUrl = organizationsUrl;
                }

                public String getReposUrl() {
                    return reposUrl;
                }

                public void setReposUrl(String reposUrl) {
                    this.reposUrl = reposUrl;
                }

                public String getEventsUrl() {
                    return eventsUrl;
                }

                public void setEventsUrl(String eventsUrl) {
                    this.eventsUrl = eventsUrl;
                }

                public String getReceivedEventsUrl() {
                    return receivedEventsUrl;
                }

                public void setReceivedEventsUrl(String receivedEventsUrl) {
                    this.receivedEventsUrl = receivedEventsUrl;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public boolean isSiteAdmin() {
                    return siteAdmin;
                }

                public void setSiteAdmin(boolean siteAdmin) {
                    this.siteAdmin = siteAdmin;
                }
            }
        }
    }

    public static class LinksBean implements Serializable{
        /**
         * self : {"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/1"}
         * html : {"href":"https://github.com/ReactiveX/BuildInfrastructure/pull/1"}
         * issue : {"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/1"}
         * comments : {"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/1/comments"}
         * review_comments : {"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/1/comments"}
         * review_comment : {"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/comments{/number}"}
         * commits : {"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/1/commits"}
         * statuses : {"href":"https://api.github.com/repos/ReactiveX/BuildInfrastructure/statuses/ac0e97acc6e69ae7554eaff4c35a3214825761fb"}
         */

        @SerializedName("self")
        private SelfBean self;
        @SerializedName("html")
        private HtmlBean html;
        @SerializedName("issue")
        private IssueBean issue;
        @SerializedName("comments")
        private CommentsBean comments;
        @SerializedName("review_comments")
        private ReviewCommentsBean reviewComments;
        @SerializedName("review_comment")
        private ReviewCommentBean reviewComment;
        @SerializedName("commits")
        private CommitsBean commits;
        @SerializedName("statuses")
        private StatusesBean statuses;

        public SelfBean getSelf() {
            return self;
        }

        public void setSelf(SelfBean self) {
            this.self = self;
        }

        public HtmlBean getHtml() {
            return html;
        }

        public void setHtml(HtmlBean html) {
            this.html = html;
        }

        public IssueBean getIssue() {
            return issue;
        }

        public void setIssue(IssueBean issue) {
            this.issue = issue;
        }

        public CommentsBean getComments() {
            return comments;
        }

        public void setComments(CommentsBean comments) {
            this.comments = comments;
        }

        public ReviewCommentsBean getReviewComments() {
            return reviewComments;
        }

        public void setReviewComments(ReviewCommentsBean reviewComments) {
            this.reviewComments = reviewComments;
        }

        public ReviewCommentBean getReviewComment() {
            return reviewComment;
        }

        public void setReviewComment(ReviewCommentBean reviewComment) {
            this.reviewComment = reviewComment;
        }

        public CommitsBean getCommits() {
            return commits;
        }

        public void setCommits(CommitsBean commits) {
            this.commits = commits;
        }

        public StatusesBean getStatuses() {
            return statuses;
        }

        public void setStatuses(StatusesBean statuses) {
            this.statuses = statuses;
        }

        public static class SelfBean implements Serializable{
            /**
             * href : https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/1
             */

            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class HtmlBean implements Serializable{
            /**
             * href : https://github.com/ReactiveX/BuildInfrastructure/pull/1
             */

            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class IssueBean implements Serializable{
            /**
             * href : https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/1
             */

            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class CommentsBean implements Serializable{
            /**
             * href : https://api.github.com/repos/ReactiveX/BuildInfrastructure/issues/1/comments
             */

            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class ReviewCommentsBean implements Serializable{
            /**
             * href : https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/1/comments
             */

            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class ReviewCommentBean implements Serializable{
            /**
             * href : https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/comments{/number}
             */

            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class CommitsBean implements Serializable{
            /**
             * href : https://api.github.com/repos/ReactiveX/BuildInfrastructure/pulls/1/commits
             */

            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }

        public static class StatusesBean implements Serializable{
            /**
             * href : https://api.github.com/repos/ReactiveX/BuildInfrastructure/statuses/ac0e97acc6e69ae7554eaff4c35a3214825761fb
             */

            @SerializedName("href")
            private String href;

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }
        }
    }

    public static class MergedByBean implements Serializable{
        /**
         * login : quidryan
         * id : 360255
         * avatar_url : https://avatars0.githubusercontent.com/u/360255?v=4
         * gravatar_id :
         * url : https://api.github.com/users/quidryan
         * html_url : https://github.com/quidryan
         * followers_url : https://api.github.com/users/quidryan/followers
         * following_url : https://api.github.com/users/quidryan/following{/other_user}
         * gists_url : https://api.github.com/users/quidryan/gists{/gist_id}
         * starred_url : https://api.github.com/users/quidryan/starred{/owner}{/repo}
         * subscriptions_url : https://api.github.com/users/quidryan/subscriptions
         * organizations_url : https://api.github.com/users/quidryan/orgs
         * repos_url : https://api.github.com/users/quidryan/repos
         * events_url : https://api.github.com/users/quidryan/events{/privacy}
         * received_events_url : https://api.github.com/users/quidryan/received_events
         * type : User
         * site_admin : false
         */

        @SerializedName("login")
        private String login;
        @SerializedName("id")
        private int id;
        @SerializedName("avatar_url")
        private String avatarUrl;
        @SerializedName("gravatar_id")
        private String gravatarId;
        @SerializedName("url")
        private String url;
        @SerializedName("html_url")
        private String htmlUrl;
        @SerializedName("followers_url")
        private String followersUrl;
        @SerializedName("following_url")
        private String followingUrl;
        @SerializedName("gists_url")
        private String gistsUrl;
        @SerializedName("starred_url")
        private String starredUrl;
        @SerializedName("subscriptions_url")
        private String subscriptionsUrl;
        @SerializedName("organizations_url")
        private String organizationsUrl;
        @SerializedName("repos_url")
        private String reposUrl;
        @SerializedName("events_url")
        private String eventsUrl;
        @SerializedName("received_events_url")
        private String receivedEventsUrl;
        @SerializedName("type")
        private String type;
        @SerializedName("site_admin")
        private boolean siteAdmin;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }

        public void setAvatarUrl(String avatarUrl) {
            this.avatarUrl = avatarUrl;
        }

        public String getGravatarId() {
            return gravatarId;
        }

        public void setGravatarId(String gravatarId) {
            this.gravatarId = gravatarId;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getHtmlUrl() {
            return htmlUrl;
        }

        public void setHtmlUrl(String htmlUrl) {
            this.htmlUrl = htmlUrl;
        }

        public String getFollowersUrl() {
            return followersUrl;
        }

        public void setFollowersUrl(String followersUrl) {
            this.followersUrl = followersUrl;
        }

        public String getFollowingUrl() {
            return followingUrl;
        }

        public void setFollowingUrl(String followingUrl) {
            this.followingUrl = followingUrl;
        }

        public String getGistsUrl() {
            return gistsUrl;
        }

        public void setGistsUrl(String gistsUrl) {
            this.gistsUrl = gistsUrl;
        }

        public String getStarredUrl() {
            return starredUrl;
        }

        public void setStarredUrl(String starredUrl) {
            this.starredUrl = starredUrl;
        }

        public String getSubscriptionsUrl() {
            return subscriptionsUrl;
        }

        public void setSubscriptionsUrl(String subscriptionsUrl) {
            this.subscriptionsUrl = subscriptionsUrl;
        }

        public String getOrganizationsUrl() {
            return organizationsUrl;
        }

        public void setOrganizationsUrl(String organizationsUrl) {
            this.organizationsUrl = organizationsUrl;
        }

        public String getReposUrl() {
            return reposUrl;
        }

        public void setReposUrl(String reposUrl) {
            this.reposUrl = reposUrl;
        }

        public String getEventsUrl() {
            return eventsUrl;
        }

        public void setEventsUrl(String eventsUrl) {
            this.eventsUrl = eventsUrl;
        }

        public String getReceivedEventsUrl() {
            return receivedEventsUrl;
        }

        public void setReceivedEventsUrl(String receivedEventsUrl) {
            this.receivedEventsUrl = receivedEventsUrl;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public boolean isSiteAdmin() {
            return siteAdmin;
        }

        public void setSiteAdmin(boolean siteAdmin) {
            this.siteAdmin = siteAdmin;
        }
    }
}
