package com.main.desafio_concrete.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.main.desafio_concrete.R;
import com.main.desafio_concrete.entity.UserPulls;
import com.main.desafio_concrete.view.RoundedTransformation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by digi on 09/11/2017.
 */

public class ProfilepullsAdapter extends RecyclerView.Adapter<ProfilepullsAdapter.ViewHolder> {
    private List<UserPulls> listProfile = new ArrayList<>();
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView userName2 , usernamerepo2, descrepo2;
        private LinearLayout item2;
        private ImageView imageView2;

        private ViewHolder(View view) {
            super(view);
            item2            = (LinearLayout)   view.findViewById(R.id.profile_recycler_adapter_layout_rl2);
            userName2        = (TextView)       view.findViewById(R.id.profile_recycler_adapter_layout_name_user2);
            usernamerepo2    = (TextView)       view.findViewById(R.id.profile_recycler_adapter_layout_name_repo2);
            descrepo2        = (TextView)       view.findViewById(R.id.profile_recycler_adapter_layout_desc_repo2);
            imageView2       = (ImageView)      view.findViewById(R.id.profile_recycler_adapter_layout_img2);
        }
    }

    public ProfilepullsAdapter(Context context, List<UserPulls> listProfile){
        this.context     = context;
        this.listProfile = listProfile;
    }


    @Override
    public ProfilepullsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profilepulls_recycler_adapter_layout, parent, false);
        return new ProfilepullsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        UserPulls profile = listProfile.get(position);

        holder.descrepo2.setText(profile.getBody());
        holder.userName2.setText(String.valueOf(profile.getUser().getId()));
        holder.usernamerepo2.setText(String.valueOf(profile.getUser().getId()));

        Picasso.with(context).load(profile.getUser().getAvatarUrl())
                .transform(new RoundedTransformation())
                .resize(110, 110)
                .centerCrop().into(holder.imageView2);

        holder.item2.setTag(profile);
        holder.item2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intentActivity = new Intent(context, LoginActivity.class);
//                intentActivity.putExtra(LoginPresenterImplements.KEY_TAG, (UserGit)v.getTag());
//                context.startActivity(intentActivity);
            }
        });
    }


    @Override
    public int getItemCount() {
        return listProfile.size();
    }
}
