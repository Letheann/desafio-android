package com.main.desafio_concrete.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.main.desafio_concrete.LoginActivity;
import com.main.desafio_concrete.R;
import com.main.desafio_concrete.entity.UserGit;
import com.main.desafio_concrete.presenter.LoginPresenterImplements;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.main.desafio_concrete.view.*;


/**
 *
 * Adapter Profile
 *
 * Created by bruno on 02/01/2017.
 */

public class ProfileRecyclerAdapter extends RecyclerView.Adapter<ProfileRecyclerAdapter.ViewHolder> {

    private List<UserGit> listProfile = new ArrayList<>();
    private Context       context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView             userName , usernamerepo, descrepo ,forks , stars;
        private LinearLayout         item;
        private ImageView            imageView;

        private ViewHolder(View view) {
            super(view);
            item             = (LinearLayout) view.findViewById(R.id.profile_recycler_adapter_layout_rl);
            userName         = (TextView)       view.findViewById(R.id.profile_recycler_adapter_layout_name_user);
            usernamerepo     = (TextView)       view.findViewById(R.id.profile_recycler_adapter_layout_name_repo);
            descrepo         = (TextView)       view.findViewById(R.id.profile_recycler_adapter_layout_desc_repo);
            forks            = (TextView)       view.findViewById(R.id.profile_recycler_apadter_forks);
            stars            = (TextView)       view.findViewById(R.id.profile_recycler_apadter_stars);
            imageView        = (ImageView)      view.findViewById(R.id.profile_recycler_adapter_layout_img);
        }
    }

    public ProfileRecyclerAdapter(Context context, List<UserGit> listProfile){
        this.context     = context;
        this.listProfile = listProfile;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_recycler_adapter_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        UserGit profile = listProfile.get(position);

        holder.userName.setText(profile.getFullName());
        holder.usernamerepo.setText(profile.getName());
        holder.descrepo.setText(profile.getDescription());
        holder.forks.setText("Forks :" + String.valueOf(profile.getForksCount()));
        holder.stars.setText("estrelas :"+String.valueOf(profile.getStargazersCount()));

        Picasso.with(context).load(profile.getOwner().getAvatarUrl())
                .transform(new RoundedTransformation())
                .resize(110, 110)
                .centerCrop().into(holder.imageView);

        holder.item.setTag(profile);
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentActivity = new Intent(context, LoginActivity.class);
                intentActivity.putExtra(LoginPresenterImplements.KEY_TAG, (UserGit)v.getTag());
                context.startActivity(intentActivity);
            }
        });

    }

    public void addItem(UserGit[] item, int position) {
        HashSet<UserGit> hash = new HashSet<>();

        for (UserGit userGit : item) {
            hash.add(userGit);
        }

        List<UserGit> userGits = new ArrayList<>();

        userGits.addAll(listProfile);
        userGits.addAll(hash);

        listProfile = userGits;


        notifyItemInserted(position);

    }


    @Override
    public int getItemCount() {
        return listProfile.size();
    }
}
