package com.main.desafio_concrete;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

import com.github.rafaelcrz.android_endless_scroll_lib.ScrollEndless;
import com.main.desafio_concrete.entity.UserGit;
import com.main.desafio_concrete.presenter.UsersPresenter;
import com.main.desafio_concrete.presenter.UsersPresenterImplements;

public class UsersActivity extends AppCompatActivity {

    private RecyclerView  recyclerView;
    private UsersPresenter usersPresenter;
    private ScrollEndless  endless;
    private LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        inti();
        load();
    }

    private void inti() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.digi_activity_usuario);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.activity_digi_recyclerview);
        layout = (LinearLayout) findViewById(R.id.activity_digi);
    }

    private void load() {


        UserGit[] arrayProfile = (UserGit[]) getIntent().getSerializableExtra(getString(R.string.load_profile_iservice_key_intent_profile));

        usersPresenter = new UsersPresenterImplements(this, recyclerView, endless, layout);

        usersPresenter.order(arrayProfile);

        usersPresenter.adapter();


    }


}
